﻿using Microsoft.EntityFrameworkCore;
using Data.Entities;

namespace Data.Initializer
{
    public static class DatabaseSeeder
    {
        public static void Seed(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category
                {
                    Id = 1,
                    Name = "Mobiles"
                },
                new Category
                {
                    Id = 2,
                    Name = "Laptops"
                },
                new Category
                {
                    Id = 3,
                    Name = "TV"
                },
                new Category
                {
                    Id = 4,
                    Name = "Tablets"
                },
                new Category
                {
                    Id = 5,
                    Name = "Smartwatches"
                },
                new Category
                {
                    Id = 6,
                    Name = "Headphones"
                },
                new Category
                {
                    Id = 7,
                    Name = "Cameras"
                }
            );
            modelBuilder.Entity<Status>().HasData(
                new Status
                {
                    Id = 1,
                    Name = "Created"
                },
                new Status
                {
                    Id = 2,
                    Name = "Canceled"
                },
                new Status
                {
                    Id = 3,
                    Name = "Canceled by admin"
                },
                new Status
                {
                    Id = 4,
                    Name = "Sent"
                }
            );
        }
    }
}
