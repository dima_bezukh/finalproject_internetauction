﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class RateRepository : IRateRepository
    {
        private readonly AuctionDbContext _context;
        public RateRepository(AuctionDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Rate entity)
        {
            await _context.Rates.AddAsync(entity);
        }

        public void Delete(Rate entity)
        {
            _context.Rates.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Rate rate = await GetByIdAsync(id);
            _context.Rates.Remove(rate);
        }

        public IQueryable<Rate> FindAll()
        {
            return _context.Rates;
        }

        public IQueryable<Rate> FindAllWithDetails()
        {
            return _context.Rates.Include(n => n.Product).Include(m => m.User).Include(p => p.Order);
        }

        public async Task<Rate> GetByIdAsync(int id)
        {
            return await _context.Rates.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<Rate> GetByIdWithDelailsAsync(int id)
        {
            return await FindAllWithDetails().FirstOrDefaultAsync(p => p.Id == id);
        }

        public void Update(Rate entity)
        {
            _context.Rates.Update(entity);
        }
    }
}
