﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class StatustRepository : IStatustRepository
    {
        private readonly AuctionDbContext _context;
        public StatustRepository(AuctionDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Status entity)
        {
            await _context.Statuses.AddAsync(entity);
        }

        public void Delete(Status entity)
        {
            _context.Statuses.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Status status = await _context.Statuses.FirstOrDefaultAsync(p => p.Id == id);
            _context.Statuses.Remove(status);
        }

        public IQueryable<Status> FindAll()
        {
            return _context.Statuses;
        }

        public IQueryable<Status> FindAllWithDetails()
        {
            return _context.Statuses.Include(n => n.Orders);
        }

        public async Task<Status> GetByIdAsync(int id)
        {
            return await _context.Statuses.FirstOrDefaultAsync(p => p.Id == id);
        }
    }
}
