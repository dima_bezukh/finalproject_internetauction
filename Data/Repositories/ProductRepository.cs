﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly AuctionDbContext _context;
        public ProductRepository(AuctionDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Product entity)
        {
            await _context.Products.AddAsync(entity);
        }

        public void Delete(Product entity)
        {
            _context.Products.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Product product = await GetByIdAsync(id);
            _context.Products.Remove(product);
        }

        public IQueryable<Product> FindAll()
        {
            return _context.Products;
        }

        public IQueryable<Product> FindAllWithDetails()
        {
            return _context.Products.Include(n => n.Category).Include(m => m.User).Include(p=>p.Rates).Include(c=>c.Images);
        }

        public async Task<Product> GetByIdAsync(int id)
        {
            return await _context.Products.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<Product> GetByIdWithDelailsAsync(int id)
        {
            return await FindAllWithDetails().FirstOrDefaultAsync(p => p.Id == id);
        }

        public void Update(Product entity)
        {
            _context.Products.Update(entity);
        }
    }
}
