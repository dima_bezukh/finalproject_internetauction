﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly AuctionDbContext _context;
        public OrderRepository(AuctionDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Order entity)
        {
            await _context.Orders.AddAsync(entity);
        }

        public void Delete(Order entity)
        {
            _context.Orders.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Order order = await GetByIdAsync(id);
            _context.Orders.Remove(order);
        }

        public IQueryable<Order> FindAll()
        {
            return _context.Orders;
        }

        public IQueryable<Order> FindAllWithDetails()
        {
            return _context.Orders.Include(p => p.Status).Include(n => n.Rate);
        }

        public async Task<Order> GetByIdAsync(int id)
        {
            return await _context.Orders.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<Order> GetByIdWithDelailsAsync(int id)
        {
            return await FindAllWithDetails().FirstOrDefaultAsync(p => p.Id == id);
        }

        public void Update(Order entity)
        {
            _context.Orders.Update(entity);
        }
    }
}
