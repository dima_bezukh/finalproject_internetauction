﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;


namespace Data.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly AuctionDbContext _context;
        public CategoryRepository(AuctionDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Category entity)
        {
            await _context.Categories.AddAsync(entity);
        }

        public void Delete(Category entity)
        {
            _context.Categories.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Category category = await _context.Categories.FirstOrDefaultAsync(p => p.Id == id);
            _context.Categories.Remove(category);
        }
        public IQueryable<Category> FindAll()
        {
            return _context.Categories;
        }
        public IQueryable<Category> FindAllWithDetails()
        {
            return _context.Categories.Include(p => p.Products);
        }

        public async Task<Category> GetByIdAsync(int id)
        {
            return await _context.Categories.FirstOrDefaultAsync(p => p.Id == id);
        }
    }
}
