﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class ImageRepository : IImageRepository
    {
        private readonly AuctionDbContext _context;
        public ImageRepository(AuctionDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Image entity)
        {
            await _context.Images.AddAsync(entity);
        }

        public void Delete(Image entity)
        {
            _context.Images.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Image image = await _context.Images.FirstOrDefaultAsync(p => p.Id == id);
            _context.Images.Remove(image);
        }

        public IQueryable<Image> FindAll()
        {
            return _context.Images;
        }
    }
}
