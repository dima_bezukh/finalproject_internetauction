﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Order: BaseEntity
    {
        public DateTime CreatingDate { get; set; }
        public string ShippingAddress { get; set; }
        public int StatusId { get; set; }
        public int RateId { get; set; }

        public Status Status { get; set; }
        public Rate Rate { get; set; }
    }
}
