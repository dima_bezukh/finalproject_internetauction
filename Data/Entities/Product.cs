﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Product: BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal InitialPrice { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime ClosingDate { get; set; }
        public int CategoryId { get; set; }
        public string UserId { get; set; }

        public Category Category { get; set; }
        public User User { get; set; }
        public ICollection<Image> Images { get; set; }
        public ICollection<Rate> Rates { get; set; }
    }
}
