﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Data.Entities
{
    public class User: IdentityUser
    {
        public decimal Balance { get; set; }
        public string Name { get; set; }
        public ICollection<Product> Products { get; set; }
        public ICollection<Rate> Rates { get; set; }
    }
}
