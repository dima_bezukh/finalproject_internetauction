﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Rate: BaseEntity
    {
        public bool IsWinning { get; set; }
        public decimal SuggestedPrice { get; set; }
        public DateTime CreatingDate { get; set; }
        public int ProductId { get; set; }
        public string UserId { get; set; }

        public Product Product { get; set; }
        public User User { get; set; }
        public Order Order { get; set; }
    }
}
