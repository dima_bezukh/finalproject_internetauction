﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Status: BaseEntity
    {
        public string Name { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
