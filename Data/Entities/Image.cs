﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Image: BaseEntity
    {
        public string ImageName { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
