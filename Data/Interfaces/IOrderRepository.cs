﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Interfaces
{
    public interface IOrderRepository: IRepository<Order>
    {
        IQueryable<Order> FindAllWithDetails();
        Task<Order> GetByIdWithDelailsAsync(int id);
    }
}
