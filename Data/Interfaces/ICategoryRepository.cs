﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Interfaces
{
    public interface ICategoryRepository
    {
        Task<Category> GetByIdAsync(int id);
        IQueryable<Category> FindAll();
        IQueryable<Category> FindAllWithDetails();
        Task AddAsync(Category entity);
        void Delete(Category entity);
        Task DeleteByIdAsync(int id);
    }
}
