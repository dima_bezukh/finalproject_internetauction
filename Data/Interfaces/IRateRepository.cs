﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Interfaces
{
    public interface IRateRepository: IRepository<Rate>
    {
        IQueryable<Rate> FindAllWithDetails();
        Task<Rate> GetByIdWithDelailsAsync(int id);
    }
}
