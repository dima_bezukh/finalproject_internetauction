﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Interfaces
{
    public interface IProductRepository: IRepository<Product>
    {
        IQueryable<Product> FindAllWithDetails();
        Task<Product> GetByIdWithDelailsAsync(int id);
    }
}
