﻿using System.Linq;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Interfaces
{
    public interface IStatustRepository
    {
        Task<Status> GetByIdAsync(int id);
        IQueryable<Status> FindAll();
        IQueryable<Status> FindAllWithDetails();
        Task AddAsync(Status entity);
        void Delete(Status entity);
        Task DeleteByIdAsync(int id);

    }
}
