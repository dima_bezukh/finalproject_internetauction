﻿using Data.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IImageRepository
    {
        IQueryable<Image> FindAll();
        Task AddAsync(Image entity);
        void Delete(Image entity);
        Task DeleteByIdAsync(int id);
    }
}
