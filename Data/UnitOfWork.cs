﻿using Data.Interfaces;
using Data.Repositories;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AuctionDbContext db;
        private ICategoryRepository categoryRepository;
        private IImageRepository imageRepository;
        private IOrderRepository orderRepository;
        private IProductRepository productRepository;
        private IRateRepository rateRepository;
        private IStatustRepository statustRepository;

        public UnitOfWork(DbContextOptions<AuctionDbContext> options)
        {
            db = new AuctionDbContext(options);
        }
        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (categoryRepository == null)
                    categoryRepository = new CategoryRepository(db);
                return categoryRepository;
            }
        }
        public IImageRepository ImageRepository
        {
            get
            {
                if (imageRepository == null)
                    imageRepository = new ImageRepository(db);
                return imageRepository;
            }
        }
        public IOrderRepository OrderRepository
        {
            get
            {
                if (orderRepository == null)
                    orderRepository = new OrderRepository(db);
                return orderRepository;
            }
        }
        public IProductRepository ProductRepository
        {
            get
            {
                if (productRepository == null)
                    productRepository = new ProductRepository(db);
                return productRepository;
            }
        }
        public IRateRepository RateRepository
        {
            get
            {
                if (rateRepository == null)
                    rateRepository = new RateRepository(db);
                return rateRepository;
            }
        }
        public IStatustRepository StatustRepository
        {
            get
            {
                if (statustRepository == null)
                    statustRepository = new StatustRepository(db);
                return statustRepository;
            }
        }
        public async Task<int> SaveAsync()
        {
            return await db.SaveChangesAsync();
        }
    }
}
