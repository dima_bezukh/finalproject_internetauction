﻿using Moq;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Business.Validation;
using NUnit.Framework;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace InternetAuction.Tests.BusinessTests
{
    public class StatusServiceTests
    {
        [Test]
        public async Task StatusService_AddAsync_AddsModel()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.StatustRepository.AddAsync(It.IsAny<Status>()));
            mockUnitOfWork.Setup(x => x.StatustRepository.FindAll()).Returns(Statuses.AsQueryable());
            var statusService = new StatusService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var status = new StatusModel { Id = 3, Name = "canceled" };

            await statusService.AddAsync(status);

            mockUnitOfWork.Verify(x => x.StatustRepository.AddAsync(It.Is<Status>(b => b.Id == status.Id && b.Name == status.Name)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        [Test]
        public void StatusService_AddAsync_ThrowsExceptionWithEmptyStatusName()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.StatustRepository.AddAsync(It.IsAny<Status>()));
            mockUnitOfWork.Setup(x => x.StatustRepository.FindAll()).Returns(Statuses.AsQueryable());
            var statusService = new StatusService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var status = new StatusModel { Id = 3, Name = "" };

            Assert.ThrowsAsync<InternetAuctionException>(() => statusService.AddAsync(status));
        }
        [Test]
        public void StatusService_AddAsync_ThrowsExceptionWithExistedStatusName()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.StatustRepository.AddAsync(It.IsAny<Status>()));
            mockUnitOfWork.Setup(x => x.StatustRepository.FindAll()).Returns(Statuses.AsQueryable());
            var statusService = new StatusService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var status = new StatusModel { Id = 3, Name = "sent" };

            Assert.ThrowsAsync<InternetAuctionException>(() => statusService.AddAsync(status));
        }
        private List<Status> Statuses = new List<Status>
        {
            new Status{ Id = 1, Name = "sent" },
            new Status{ Id = 2, Name = "received" }
        };
    }
}
