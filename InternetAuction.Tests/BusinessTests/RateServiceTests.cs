﻿using Moq;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Business.Validation;
using NUnit.Framework;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System;


namespace InternetAuction.Tests.BusinessTests
{
    public class RateServiceTests
    {
        [Test]
        public async Task RateService_AddAsync_AddsModel()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(Products[0]));
            mockUnitOfWork.Setup(x => x.RateRepository.AddAsync(It.IsAny<Rate>()));
            mockUnitOfWork.Setup(x => x.RateRepository.FindAll()).Returns(Rates.AsQueryable());
            var rateService = new RateService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var rate = new RateModel { Id = 100, IsWinning = false, SuggestedPrice = 125, CreatingDate = new DateTime(2020, 09, 4), ProductId = 1, UserId = "1" };

            await rateService.AddAsync(rate);

            mockUnitOfWork.Verify(x => x.RateRepository.AddAsync(It.Is<Rate>(b =>
                b.Id == rate.Id && b.IsWinning == rate.IsWinning && b.SuggestedPrice == rate.SuggestedPrice && b.CreatingDate == rate.CreatingDate &&
                b.ProductId == rate.ProductId &&  b.UserId == rate.UserId
            )), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public void RateService_AddAsync_ThrowsExceptionWithSmallerSuggestedPrice()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(Products[0]));
            mockUnitOfWork.Setup(x => x.RateRepository.AddAsync(It.IsAny<Rate>()));
            mockUnitOfWork.Setup(x => x.RateRepository.FindAll()).Returns(Rates.AsQueryable());
            var rateService = new RateService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var rate = new RateModel { Id = 100, IsWinning = false, SuggestedPrice = 99, CreatingDate = new DateTime(2020, 09, 4), ProductId = 1, UserId = "1" };

            Assert.ThrowsAsync<InternetAuctionException>(() => rateService.AddAsync(rate));
        }
        [Test]
        public void RateService_GetAll()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.RateRepository.AddAsync(It.IsAny<Rate>()));
            mockUnitOfWork.Setup(x => x.RateRepository.FindAll()).Returns(Rates.AsQueryable());
            var rateService = new RateService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var rates = rateService.GetAll();

            Assert.That(rates, Is.EqualTo(ExpectedAllRates).Using(new RateModelEqualityComparer()));
        }

        [Test]
        public async Task RateService_SetWinningRateByProductId()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.RateRepository.FindAll()).Returns(Rates.AsQueryable());
            mockUnitOfWork.Setup(x => x.RateRepository.Update(It.IsAny<Rate>()));
            var rateService = new RateService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var rate = await rateService.SetWinningRateByProductId(1);
            var expectedRate = ExpectedWinningRateByproductId1.FirstOrDefault();

            Assert.That(rate, Is.EqualTo(expectedRate).Using(new RateModelEqualityComparer()));
            mockUnitOfWork.Verify(x => x.RateRepository.Update(It.Is<Rate>(b =>
                b.Id == expectedRate.Id && b.IsWinning == expectedRate.IsWinning && b.SuggestedPrice == expectedRate.SuggestedPrice && 
                b.CreatingDate == expectedRate.CreatingDate &&
                b.ProductId == expectedRate.ProductId && b.UserId == expectedRate.UserId
            )), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task RateService_GetByIsAsync(int id)
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.RateRepository.GetByIdAsync(id)).Returns(Task.FromResult(Rates.FirstOrDefault(p=>p.Id == id)));
            var rateService = new RateService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var rate = await rateService.GetByIdAsync(id);

            Assert.That(rate, Is.EqualTo(ExpectedAllRates.FirstOrDefault(p => p.Id == id)).Using(new RateModelEqualityComparer()));
        }

        [Test]
        public async Task RateService_DeleteByIdAsync()
        {
            var rate = Rates.FirstOrDefault();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.RateRepository.GetByIdWithDelailsAsync(rate.Id)).Returns(Task.FromResult(rate));
            mockUnitOfWork.Setup(x => x.RateRepository.DeleteByIdAsync(It.IsAny<int>()));
            var rateService = new RateService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            await rateService.DeleteByIdAsync(rate.Id);

            mockUnitOfWork.Verify(x => x.RateRepository.DeleteByIdAsync(It.Is<int>(b=>b == rate.Id)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        private static List<User> Users = new List<User>
        {
            new User {Id ="1", Balance=111, UserName = "Oleg" },
            new User {Id ="2", Balance=222, UserName = "Ivan" }
        };
        private static List<Category> Categories = new List<Category>
        {
            new Category { Id = 1, Name = "phones" },
            new Category {Id = 2, Name = "documents"}
        };
        private static List<Image> Images = new List<Image>
        {
            new Image {Id = 1, ImageName = "1.jpg", ProductId = 1},
            new Image {Id = 2, ImageName = "2.jpg", ProductId = 2},
            new Image {Id = 3, ImageName = "3.jpg", ProductId = 1}
        };
        private static List<Product> Products = new List<Product>
        {
            new Product {Id = 1, Name = "name1", ClosingDate = DateTime.Now.AddDays(1), IsAvailable = true, Description = "", InitialPrice = 100, CategoryId = 1, Category = Categories[0], UserId = "1", User = Users[0]},
            new Product {Id = 2, Name = "name2", IsAvailable = true, Description = "", InitialPrice = 110, CategoryId = 2, Category = Categories[1], UserId = "1", User = Users[0]},
            new Product {Id = 3, Name = "name1", IsAvailable = true, Description = "", InitialPrice = 220, CategoryId = 2, Category = Categories[1], UserId = "2", User = Users[1]},
            new Product {Id = 4, Name = "name1", IsAvailable = false, Description = "", InitialPrice = 130, CategoryId = 2, Category = Categories[1], UserId = "1", User = Users[0]}

        };
        private static List<Rate> Rates = new List<Rate>
        {
            new Rate { Id = 1, IsWinning = false, SuggestedPrice = 110, CreatingDate = new DateTime(2020, 08, 30), Product = Products[0], ProductId = 1, UserId = "1" },
            new Rate { Id = 2, IsWinning = false, SuggestedPrice = 1100, CreatingDate = new DateTime(2020, 08, 30), Product = Products[1], ProductId = 2, UserId = "1" },
            new Rate { Id = 3, IsWinning = false, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), Product = Products[0], ProductId = 1, UserId = "2" }
        };
        private static List<RateModel> ExpectedWinningRateByproductId1 = new List<RateModel>
        {
            new RateModel { Id = 3, IsWinning = true, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "2" }
        };
        private static List<RateModel> ExpectedAllRates = new List<RateModel>
        {
            new RateModel { Id = 1, IsWinning = false, SuggestedPrice = 110, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "1" },
            new RateModel { Id = 2, IsWinning = false, SuggestedPrice = 1100, CreatingDate = new DateTime(2020, 08, 30), ProductId = 2, UserId = "1" },
            new RateModel { Id = 3, IsWinning = false, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "2" }

        };
        private static List<RateModel> ExpectedRatesModel = new List<RateModel>
        {
            new RateModel { Id = 1, IsWinning = false, SuggestedPrice = 110, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "1" },
            new RateModel { Id = 3, IsWinning = false, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "2" }
        };
        private static List<ProductModel> ExpectedProductsModel = new List<ProductModel>
        {
            new ProductModel {Id = 1, Name = "name1", IsAvailable = true, Description = "", InitialPrice = "100", CategoryId = 1, UserId = "1"},
            new ProductModel {Id = 2, Name = "name2", IsAvailable = true, Description = "", InitialPrice = "110", CategoryId = 2, UserId = "1"},
            new ProductModel {Id = 3, Name = "name1", IsAvailable = true, Description = "", InitialPrice = "220", CategoryId = 2, UserId = "2"}
        };
        private static List<ImageModel> ExpectedImages = new List<ImageModel>
        {
            new ImageModel {Id = 1, ImageName = "1.jpg", ProductId = 1},
            new ImageModel {Id = 3, ImageName = "3.jpg", ProductId = 1}
        };
    }
}
