﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Business.Validation;
using NUnit.Framework;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Entities;

namespace InternetAuction.Tests.BusinessTests
{
    public class ImageServiceTests
    {
        [Test]
        public async Task ImageService_AddAsync_AddsModel()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ImageRepository.AddAsync(It.IsAny<Image>()));
            mockUnitOfWork.Setup(x => x.ProductRepository.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(Products[0]));
            var imageService = new ImageService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var imageModel = new ImageModel { ImageName = "123.jpg", ProductId = 1 };

            string res = await imageService.AddAsync(imageModel);

            mockUnitOfWork.Verify(x => x.ImageRepository.AddAsync(It.IsAny<Image>()), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        [Test]
        public void ImageService_AddAsync_ThrowsExceptionWithEmptyFileName()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ImageRepository.AddAsync(It.IsAny<Image>()));
            mockUnitOfWork.Setup(x => x.ProductRepository.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(Products[0]));
            var imageService = new ImageService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var imageModel = new ImageModel { ImageName = "", ProductId = 1 };

            Assert.ThrowsAsync<InternetAuctionException>(() => imageService.AddAsync(imageModel));
        }
        [Test]
        public void ImageService_AddAsync_ThrowsExceptionWrongFormat()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ImageRepository.AddAsync(It.IsAny<Image>()));
            mockUnitOfWork.Setup(x => x.ProductRepository.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(Products[0]));
            var imageService = new ImageService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var imageModel = new ImageModel { ImageName = "123.txt", ProductId = 1 };

            Assert.ThrowsAsync<InternetAuctionException>(() => imageService.AddAsync(imageModel));
        }

        [Test]
        public void ImageService_AddAsync_ThrowsExceptionWrongProductId()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ImageRepository.AddAsync(It.IsAny<Image>()));
            mockUnitOfWork.Setup(x => x.ProductRepository.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult(Products[0]));
            var imageService = new ImageService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var imageModel = new ImageModel { ImageName = "123.img", ProductId = -1 };

            Assert.ThrowsAsync<InternetAuctionException>(() => imageService.AddAsync(imageModel));
        }
        private static List<Product> Products = new List<Product>
        {
            new Product {Id = 1, Name = "name1", IsAvailable = true, Description = "", InitialPrice = 100, CategoryId = 1, UserId = "1"},
        };
    }
}
