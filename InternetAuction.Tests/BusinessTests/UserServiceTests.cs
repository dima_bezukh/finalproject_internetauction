﻿using Moq;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Business.Validation;
using NUnit.Framework;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using FakeItEasy;

namespace InternetAuction.Tests.BusinessTests
{
    public class UserServiceTests
    {
        private Mock<SignInManager<User>> GetMockSignInManager()
        {
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            var ctxAccessor = new HttpContextAccessor();
            var mockClaimsPrinFact = new Mock<IUserClaimsPrincipalFactory<User>>();
            var mockOpts = new Mock<IOptions<IdentityOptions>>();
            var mockLogger = new Mock<ILogger<SignInManager<User>>>();

            return new Mock<SignInManager<User>>(mockUser.Object, ctxAccessor, mockClaimsPrinFact.Object, mockOpts.Object, mockLogger.Object);
        }
        [Test]
        public void UserService_GetOrdersByUserId()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.OrderRepository.FindAllWithDetails()).Returns(Orders.AsQueryable());
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            var signInManager = A.Fake<SignInManager<User>>();
            var userService = new UserService(mockUser.Object, mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), signInManager);

            var orders = userService.GetOrdersByUserId("1");
            
            Assert.That(orders, Is.EqualTo(ExpectedOrdersModel).Using(new OrderModelEqualityComparer()));
        }

        [Test]
        public void UserService_GetWinningLotsByUserId()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.RateRepository.FindAllWithDetails()).Returns(Rates.AsQueryable());
            mockUnitOfWork.Setup(x => x.OrderRepository.FindAll()).Returns(Orders.AsQueryable());
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            var signInManager = A.Fake<SignInManager<User>>();
            var userService = new UserService(mockUser.Object, mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), signInManager);

            var lots = userService.GetWonLotsByUserId("1");

            Assert.That(lots, Is.EqualTo(ExpectedProductsModel).Using(new ProductModelEqualityComparer()));
        }

        [Test]
        public void UserService_GetUsersLots()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.FindAll()).Returns(Products.AsQueryable());
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            var signInManager = A.Fake<SignInManager<User>>();
            var userService = new UserService(mockUser.Object, mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), signInManager);

            var lots = userService.GetUsersProducts("1");

            Assert.That(lots, Is.EqualTo(ExpectedUsersProductsModel).Using(new ProductModelEqualityComparer()));
        }

        [Test]
        public async Task UserService_RechargeUsersBalance()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            var signInManager = A.Fake<SignInManager<User>>();
            var userService = new UserService(mockUser.Object, mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), signInManager); 
            PaymentDetailModel paymentmodel = new PaymentDetailModel
            {
                AmountOfMoney = "100",
                CardNumber = "1234567890123456",
                ExpirationDate = "12/27",
                SecurityCode = "123",
                UserId = "1"
            };

            await userService.RechargeUsersBalance(paymentmodel);

            Assert.Pass();
        }

        [Test]
        public void UserService_RechargeUsersBalance_ThrowsException_InvalidCardNumber()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            var signInManager = A.Fake<SignInManager<User>>();
            var userService = new UserService(mockUser.Object, mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), signInManager);
            PaymentDetailModel paymentmodel = new PaymentDetailModel
            {
                AmountOfMoney = "100",
                CardNumber = "123456789012345A",
                ExpirationDate = "12/27",
                SecurityCode = "123",
                UserId = "1"
            };

            Assert.ThrowsAsync<InternetAuctionException>(() => userService.RechargeUsersBalance(paymentmodel));
        }
        [Test]
        public void UserService_RechargeUsersBalance_ThrowsException_InvalidExpirationDate()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            var signInManager = A.Fake<SignInManager<User>>();
            var userService = new UserService(mockUser.Object, mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), signInManager);
            PaymentDetailModel paymentmodel = new PaymentDetailModel
            {
                AmountOfMoney = "100",
                CardNumber = "1234567890123456",
                ExpirationDate = "12/AA",
                SecurityCode = "123",
                UserId = "1"
            };

            Assert.ThrowsAsync<InternetAuctionException>(() => userService.RechargeUsersBalance(paymentmodel));
        }
        [Test]
        public void UserService_RechargeUsersBalance_ThrowsException_ExpiredDate()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            var signInManager = A.Fake<SignInManager<User>>();
            var userService = new UserService(mockUser.Object, mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), signInManager);
            PaymentDetailModel paymentmodel = new PaymentDetailModel
            {
                AmountOfMoney = "100",
                CardNumber = "1234567890123456",
                ExpirationDate = "12/19",
                SecurityCode = "123",
                UserId = "1"
            };

            Assert.ThrowsAsync<InternetAuctionException>(() => userService.RechargeUsersBalance(paymentmodel));
        }
        [Test]
        public void UserService_RechargeUsersBalance_ThrowsException_InvalidSecurityCode()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            var signInManager = A.Fake<SignInManager<User>>();
            var userService = new UserService(mockUser.Object, mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), signInManager);
            PaymentDetailModel paymentmodel = new PaymentDetailModel
            {
                AmountOfMoney = "100",
                CardNumber = "1234567890123456",
                ExpirationDate = "12/27",
                SecurityCode = "12A",
                UserId = "1"
            };

            Assert.ThrowsAsync<InternetAuctionException>(() => userService.RechargeUsersBalance(paymentmodel));
        }
        [Test]
        public void UserService_RechargeUsersBalance_ThrowsException_InvalidAmountOfMoney()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            var signInManager = A.Fake<SignInManager<User>>();
            var userService = new UserService(mockUser.Object, mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), signInManager);
            PaymentDetailModel paymentmodel = new PaymentDetailModel
            {
                AmountOfMoney = "-100",
                CardNumber = "1234567890123456",
                ExpirationDate = "12/27",
                SecurityCode = "123",
                UserId = "1"
            };

            Assert.ThrowsAsync<InternetAuctionException>(() => userService.RechargeUsersBalance(paymentmodel));
        }
        [Test]
        public void UserService_RechargeUsersBalance_ThrowsException_InvalidFormatAmountOfMoney()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            var signInManager = A.Fake<SignInManager<User>>();
            var userService = new UserService(mockUser.Object, mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), signInManager);
            PaymentDetailModel paymentmodel = new PaymentDetailModel
            {
                AmountOfMoney = "100A",
                CardNumber = "1234567890123456",
                ExpirationDate = "12/27",
                SecurityCode = "123",
                UserId = "1"
            };

            Assert.ThrowsAsync<InternetAuctionException>(() => userService.RechargeUsersBalance(paymentmodel));
        }
        private static List<User> Users = new List<User>
        {
            new User {Id ="1", Balance=111, UserName = "Oleg" },
            new User {Id ="2", Balance=222, UserName = "Ivan" }
        };
        private static List<Category> Categories = new List<Category>
        {
            new Category { Id = 1, Name = "phones" },
            new Category {Id = 2, Name = "documents"}
        };
        private static List<Status> Statuses = new List<Status>
        {
            new Status {Id = 1, Name = "sent"},
            new Status {Id = 2, Name = "received"}
        };
        private static List<Product> Products = new List<Product>
        {
            new Product {Id = 1, Name = "name1", Description = "", ClosingDate = new DateTime(2020, 08, 30), InitialPrice = 100, CategoryId = 1, Category = Categories[0], UserId = "1", User = Users[0]},
            new Product {Id = 2, Name = "name2", Description = "", ClosingDate = new DateTime(2020, 08, 30), InitialPrice = 110, CategoryId = 2, Category = Categories[1], UserId = "1", User = Users[0]},
            new Product {Id = 3, Name = "name1", Description = "", ClosingDate = new DateTime(2020, 08, 30), InitialPrice = 220, CategoryId = 2, Category = Categories[1], UserId = "2", User = Users[1]}
        };
        private static List<ProductModel> ExpectedProductsModel = new List<ProductModel>
        {
            new ProductModel {Id = 2, Name = "name2", Description = "", ClosingDate = new DateTime(2020, 08, 30), InitialPrice = "110", CategoryId = 2, UserId = "1"},
        };
        private static List<ProductModel> ExpectedUsersProductsModel = new List<ProductModel>
        {
            new ProductModel {Id = 1, Name = "name1", Description = "", ClosingDate = new DateTime(2020, 08, 30), InitialPrice = "100", CategoryId = 1, UserId = "1"},
            new ProductModel {Id = 2, Name = "name2", Description = "", ClosingDate = new DateTime(2020, 08, 30), InitialPrice = "110", CategoryId = 2, UserId = "1"},
        };
        private static List<Rate> Rates = new List<Rate>
        {
            new Rate { Id = 1, IsWinning = false, SuggestedPrice = 110, CreatingDate = new DateTime(2020, 08, 30), Product = Products[0], ProductId = 1, UserId = "1" },
            new Rate { Id = 2, IsWinning = true, SuggestedPrice = 130, CreatingDate = new DateTime(2020, 08, 30), Product = Products[0], ProductId = 1, UserId = "1" },
            new Rate { Id = 3, IsWinning = false, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), Product = Products[0], ProductId = 1, UserId = "2" },
            new Rate { Id = 4, IsWinning = true, SuggestedPrice = 130, CreatingDate = new DateTime(2020, 08, 30), Product = Products[1], ProductId = 2, UserId = "1" }
        };
        private static List<Order> Orders = new List<Order>
        {
            new Order {Id=2, ShippingAddress="", RateId=2, CreatingDate = new DateTime(2020, 09, 1), Rate = Rates[1], StatusId=1, Status = Statuses[0] }
        };
        private static List<OrderModel> ExpectedOrdersModel = new List<OrderModel>
        {
            new OrderModel {Id=2, ShippingAddress="", RateId=2, CreatingDate = new DateTime(2020, 09, 1), StatusId=1 }
        };
    }
}
