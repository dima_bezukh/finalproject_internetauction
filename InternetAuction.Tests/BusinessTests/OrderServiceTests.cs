﻿using Moq;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Business.Validation;
using NUnit.Framework;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.AspNetCore.Identity;

namespace InternetAuction.Tests.BusinessTests
{
    public class OrderServiceTests
    {
        [Test]
        public async Task OrderService_AddAsync_AddsModel()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            mockUnitOfWork.Setup(x => x.OrderRepository.AddAsync(It.IsAny<Order>()));
            mockUnitOfWork.Setup(x => x.OrderRepository.FindAll()).Returns(Orders.AsQueryable());
            mockUnitOfWork.Setup(x => x.StatustRepository.FindAll()).Returns(Statuses.AsQueryable());
            mockUnitOfWork.Setup(x => x.RateRepository.FindAll()).Returns(Rates.AsQueryable());
            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), mockUser.Object);
            var order = new OrderModel { Id = 3, ShippingAddress = "Cherassy", RateId = 4, CreatingDate = new DateTime(2020, 09, 1), StatusId = 3 };

            await orderService.AddAsync(order);

            mockUnitOfWork.Verify(x => x.OrderRepository.AddAsync(It.Is<Order>(b =>
                b.Id == order.Id &&
                b.ShippingAddress == order.ShippingAddress &&
                b.CreatingDate == order.CreatingDate &&
                b.RateId == order.RateId &&
                b.StatusId == order.StatusId
            )), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public void OrderService_AddAsync_ThrowsExceptionWithEmptyShippingAddress()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            mockUnitOfWork.Setup(x => x.OrderRepository.AddAsync(It.IsAny<Order>()));
            mockUnitOfWork.Setup(x => x.OrderRepository.FindAll()).Returns(Orders.AsQueryable());
            mockUnitOfWork.Setup(x => x.StatustRepository.FindAll()).Returns(Statuses.AsQueryable());
            mockUnitOfWork.Setup(x => x.RateRepository.FindAll()).Returns(Rates.AsQueryable());
            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), mockUser.Object);
            var order = new OrderModel { Id = 3, ShippingAddress = "", RateId = 4, CreatingDate = new DateTime(2020, 09, 1), StatusId = 3 };

            Assert.ThrowsAsync<InternetAuctionException>(() => orderService.AddAsync(order));
        }

        [Test]
        public void OrderService_AddAsync_ThrowsExceptionToTheLotWithAlreadyExistedOrder()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            mockUnitOfWork.Setup(x => x.OrderRepository.AddAsync(It.IsAny<Order>()));
            mockUnitOfWork.Setup(x => x.OrderRepository.FindAll()).Returns(Orders.AsQueryable());
            mockUnitOfWork.Setup(x => x.StatustRepository.FindAll()).Returns(Statuses.AsQueryable());
            mockUnitOfWork.Setup(x => x.RateRepository.FindAll()).Returns(Rates.AsQueryable());
            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), mockUser.Object);
            var order = new OrderModel { Id = 3, ShippingAddress = "", RateId = 3, CreatingDate = new DateTime(2020, 09, 1), StatusId = 3 };

            Assert.ThrowsAsync<InternetAuctionException>(() => orderService.AddAsync(order));
        }

        [Test]
        public void OrderService_AddAsync_ThrowsExceptionUserDontHaveEnoughMoney()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            mockUnitOfWork.Setup(x => x.OrderRepository.AddAsync(It.IsAny<Order>()));
            mockUnitOfWork.Setup(x => x.OrderRepository.FindAll()).Returns(Orders.AsQueryable());
            mockUnitOfWork.Setup(x => x.StatustRepository.FindAll()).Returns(Statuses.AsQueryable());
            mockUnitOfWork.Setup(x => x.RateRepository.FindAll()).Returns(Rates.AsQueryable());
            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), mockUser.Object);
            var order = new OrderModel { Id = 3, ShippingAddress = "", RateId = 2, CreatingDate = new DateTime(2020, 09, 1), StatusId = 3 };

            Assert.ThrowsAsync<InternetAuctionException>(() => orderService.AddAsync(order));
        }

        [Test]
        public void OrderService_GetAll()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            mockUnitOfWork.Setup(x => x.OrderRepository.FindAll()).Returns(Orders.AsQueryable());
            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), mockUser.Object);

            var orders = orderService.GetAll();

            Assert.That(orders, Is.EqualTo(ExpectedOrdersModel).Using(new OrderModelEqualityComparer()));
        }

        [TestCase(1)]
        [TestCase(2)]
        public async Task OrderService_GetByIsAsync(int id)
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            mockUnitOfWork.Setup(x => x.OrderRepository.GetByIdAsync(id)).Returns(Task.FromResult(Orders.FirstOrDefault(p => p.Id == id)));
            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), mockUser.Object);

            var order = await orderService.GetByIdAsync(id);

            Assert.That(order, Is.EqualTo(ExpectedOrdersModel.FirstOrDefault(p => p.Id == id)).Using(new OrderModelEqualityComparer()));
        }

        [Test]
        public async Task OrderService_UpdateModel()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            mockUnitOfWork.Setup(x => x.OrderRepository.Update(It.IsAny<Order>()));
            mockUnitOfWork.Setup(x => x.StatustRepository.FindAll()).Returns(Statuses.AsQueryable());
            mockUnitOfWork.Setup(x => x.RateRepository.FindAll()).Returns(Rates.AsQueryable());
            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), mockUser.Object);
            var order = new OrderModel { Id = 3, ShippingAddress = "Cherassy", RateId = 4, CreatingDate = new DateTime(2020, 09, 1), StatusId = 3 };

            await orderService.UpdateAsync(order);

            mockUnitOfWork.Verify(x => x.OrderRepository.Update(It.Is<Order>(b =>
                b.Id == order.Id &&
                b.ShippingAddress == order.ShippingAddress &&
                b.CreatingDate == order.CreatingDate &&
                b.RateId == order.RateId &&
                b.StatusId == order.StatusId
            )), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        [Test]
        public async Task OrderService_DeleteByIdAsync()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            var userStoreMock = new Mock<IUserStore<User>>();
            var mockUser = new Mock<UserManager<User>>(userStoreMock.Object,
                null, null, null, null, null, null, null, null);
            mockUser.Setup(um => um.FindByIdAsync(It.IsAny<string>()))
                      .Returns(Task.FromResult(Users[0]));
            mockUnitOfWork.Setup(x => x.OrderRepository.DeleteByIdAsync(It.IsAny<int>()));
            var orderService = new OrderService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile(), mockUser.Object);
            var order = Orders.FirstOrDefault();

            await orderService.DeleteByIdAsync(order.Id);

            mockUnitOfWork.Verify(x => x.OrderRepository.DeleteByIdAsync(It.Is<int>(b => b == order.Id)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        private static List<User> Users = new List<User>
        {
            new User {Id ="1", Balance=190, UserName = "Oleg" },
            new User {Id ="2", Balance=222, UserName = "Ivan" }
        };
        private static List<Category> Categories = new List<Category>
        {
            new Category { Id = 1, Name = "phones" },
            new Category {Id = 2, Name = "documents"}
        };
        private static List<Status> Statuses = new List<Status>
        {
            new Status { Id = 1, Name = "sent" },
            new Status { Id = 2, Name = "received" },
            new Status { Id = 3, Name = "created" }
        };
        private static List<Product> Products = new List<Product>
        {
            new Product {Id = 1, Name = "name1", Description = "", ClosingDate = new DateTime(2020, 08, 30), InitialPrice = 100, CategoryId = 1, Category = Categories[0], UserId = "1", User = Users[0]},
            new Product {Id = 2, Name = "name2", Description = "", ClosingDate = new DateTime(2020, 08, 30), InitialPrice = 110, CategoryId = 2, Category = Categories[1], UserId = "1", User = Users[0]},
            new Product {Id = 3, Name = "name1", Description = "", ClosingDate = new DateTime(2020, 08, 30), InitialPrice = 220, CategoryId = 2, Category = Categories[1], UserId = "2", User = Users[1]}
        };
        private static List<Rate> Rates = new List<Rate>
        {
            new Rate { Id = 1, IsWinning = false, SuggestedPrice = 110, CreatingDate = new DateTime(2020, 08, 30), Product = Products[0], ProductId = 1, UserId = "1" },
            new Rate { Id = 2, IsWinning = true, SuggestedPrice = 1100, CreatingDate = new DateTime(2020, 08, 30), Product = Products[1], ProductId = 2, UserId = "1" },
            new Rate { Id = 3, IsWinning = true, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), Product = Products[0], ProductId = 1, UserId = "2" },
            new Rate { Id = 4, IsWinning = true, SuggestedPrice = 170, CreatingDate = new DateTime(2020, 08, 30), Product = Products[2], ProductId = 3, UserId = "2" }

        };
        private static List<Order> Orders = new List<Order>
        {
            new Order {Id=1, ShippingAddress="", RateId=2, CreatingDate = new DateTime(2020, 09, 1), Rate = Rates[1], StatusId=1, Status = Statuses[0] },
            new Order {Id=2, ShippingAddress="", RateId=3, CreatingDate = new DateTime(2020, 09, 1), Rate = Rates[2], StatusId=3, Status = Statuses[2] }
        };
        private static List<OrderModel> ExpectedOrdersModel = new List<OrderModel>
        {
            new OrderModel {Id=1, ShippingAddress="", RateId=2, CreatingDate = new DateTime(2020, 09, 1), StatusId=1 },
            new OrderModel {Id=2, ShippingAddress="", RateId=3, CreatingDate = new DateTime(2020, 09, 1), StatusId=3 }
        };
    }
}
