﻿using Moq;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Business.Validation;
using NUnit.Framework;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System;

namespace InternetAuction.Tests.BusinessTests
{
    public class ProductServiceTests
    {
        [Test]
        public async Task ProductService_AddAsync_AddsModel()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.AddAsync(It.IsAny<Product>()));
            mockUnitOfWork.Setup(x => x.CategoryRepository.FindAll()).Returns(Categories.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var product = new ProductModel
            {
                Id = 1,
                Name = "product",
                Description = "",
                InitialPrice = "100",
                ClosingDate = DateTime.Now.AddDays(2),
                CategoryId = 1,
                UserId = "123"
            };

            await productService.AddAsync(product);

            mockUnitOfWork.Verify(x => x.ProductRepository.AddAsync(It.Is<Product>(b =>
                b.Id == product.Id &&
                b.Name == product.Name &&
                b.Description == product.Description &&
                b.InitialPrice == Convert.ToDecimal(product.InitialPrice) &&
                b.ClosingDate == product.ClosingDate &&
                b.CategoryId == product.CategoryId &&
                b.UserId == product.UserId
            )), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        [Test]
        public void ProductService_AddAsync_ThrowsExceptionWithEmptyName()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.AddAsync(It.IsAny<Product>()));
            mockUnitOfWork.Setup(x => x.CategoryRepository.FindAll()).Returns(Categories.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var product = new ProductModel
            {
                Id = 1,
                Name = "",
                Description = "",
                InitialPrice = "100",
                ClosingDate = DateTime.Now.AddDays(2),
                CategoryId = 1,
                UserId = "123"
            };

            Assert.ThrowsAsync<InternetAuctionException>(() => productService.AddAsync(product));
        }
        [Test]
        public void ProductService_AddAsync_ThrowsExceptionWithWrongInitilaPrice()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.AddAsync(It.IsAny<Product>()));
            mockUnitOfWork.Setup(x => x.CategoryRepository.FindAll()).Returns(Categories.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var product = new ProductModel
            {
                Id = 1,
                Name = "samsung",
                Description = "",
                InitialPrice = "-100",
                ClosingDate = DateTime.Now.AddDays(2),
                CategoryId = 1,
                UserId = "123"
            };

            Assert.ThrowsAsync<InternetAuctionException>(() => productService.AddAsync(product));
        }
        [Test]
        public void ProductService_AddAsync_ThrowsExceptionWithWrongClosingDate()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.AddAsync(It.IsAny<Product>()));
            mockUnitOfWork.Setup(x => x.CategoryRepository.FindAll()).Returns(Categories.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var product = new ProductModel
            {
                Id = 1,
                Name = "samsung",
                Description = "",
                InitialPrice = "100",
                ClosingDate = new DateTime(2021, 9, 1),
                CategoryId = 1,
                UserId = "123"
            };

            Assert.ThrowsAsync<InternetAuctionException>(() => productService.AddAsync(product));
        }
        [Test]
        public void ProductService_AddAsync_ThrowsExceptionWithWrongCategoryId()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.AddAsync(It.IsAny<Product>()));
            mockUnitOfWork.Setup(x => x.CategoryRepository.FindAll()).Returns(Categories.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var product = new ProductModel
            {
                Id = 1,
                Name = "samsung",
                Description = "",
                InitialPrice = "100",
                ClosingDate = DateTime.Now.AddDays(2),
                CategoryId = 0,
                UserId = "123"
            };

            Assert.ThrowsAsync<InternetAuctionException>(() => productService.AddAsync(product));
        }
        [Test]
        public void ProductService_DeleteByIdAsync_ThrowsExceptionWithWrongId()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.DeleteByIdAsync(It.IsAny<int>()));
            mockUnitOfWork.Setup(x => x.ProductRepository.GetByIdAsync(It.IsAny<int>())).Returns(Task.FromResult<Product>(null));
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            Assert.ThrowsAsync<InternetAuctionException>(() => productService.DeleteByIdAsync(1));
        }

        [Test]
        public void ProductService_DeleteByIdAsync_ThrowsExceptionWithWrongOwnerId()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.DeleteByIdAsync(It.IsAny<int>()));
            mockUnitOfWork.Setup(x => x.ProductRepository.GetByIdAsync(1)).Returns(Task.FromResult<Product>(Products[0]));
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            Assert.ThrowsAsync<InternetAuctionException>(() => productService.DeleteByIdAsyncUseOwnerId(1, "2"));
        }

        [Test]
        public void ProductService_GetAll()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.FindAll()).Returns(Products.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var products = productService.GetAll();

            Assert.That(products, Is.EqualTo(ExpectedProductsModel).Using(new ProductModelEqualityComparer()));
        }

        [Test]
        public void ProductService_GetAllImagesByProductId()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ImageRepository.FindAll()).Returns(Images.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var images = productService.GetAllImagesByProductId(1);

            Assert.That(images, Is.EqualTo(ExpectedImages).Using(new ImageModelEqualityComparer()));
        }

        [Test]
        public void ProductService_GetByFilter1()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.FindAllWithDetails()).Returns(Products.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            FilterSearchModel filterSearch = new FilterSearchModel
            {
                ProductName = "",
                MinInitialPrice = 105,
                MaxInitialPrice = 220,
                Seller = "Oleg",
                CategoryName = ""
            };

            var products = productService.GetByFilter(filterSearch);

            Assert.That(products, Is.EqualTo(ExpectedProductsModelUsingFilter1).Using(new ProductModelEqualityComparer()));
        }
        [Test]
        public void ProductService_GetByFilter2()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.FindAllWithDetails()).Returns(Products.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            FilterSearchModel filterSearch = new FilterSearchModel
            {
                ProductName = "",
                MinInitialPrice = 100,
                MaxInitialPrice = 120,
                Seller = "Oleg",
                CategoryName = "documents"
            };

            var products = productService.GetByFilter(filterSearch);

            Assert.That(products, Is.EqualTo(ExpectedProductsModelUsingFilter2).Using(new ProductModelEqualityComparer()));
        }
        [Test]
        public void ProductService_GetByFilter3()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.FindAllWithDetails()).Returns(Products.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            FilterSearchModel filterSearch = new FilterSearchModel
            {
                ProductName = "name1",
                MinInitialPrice = 100,
                MaxInitialPrice = 320,
                Seller = "",
                CategoryName = "documents"
            };

            var products = productService.GetByFilter(filterSearch);

            Assert.That(products, Is.EqualTo(ExpectedProductsModelUsingFilter3).Using(new ProductModelEqualityComparer()));
        }

        [Test]
        public async Task ProductService_Update_UpdateModel()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ProductRepository.Update(It.IsAny<Product>()));
            mockUnitOfWork.Setup(x => x.CategoryRepository.FindAll()).Returns(Categories.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var product = new ProductModel
            {
                Id = 1,
                Name = "product",
                Description = "",
                InitialPrice = "100",
                ClosingDate = DateTime.Now.AddDays(2),
                CategoryId = 1,
                UserId = "123"
            };

            await productService.UpdateAsync(product);

            mockUnitOfWork.Verify(x => x.ProductRepository.Update(It.Is<Product>(b =>
                b.Id == product.Id &&
                b.Name == product.Name &&
                b.Description == product.Description &&
                b.InitialPrice == Convert.ToDecimal(product.InitialPrice) &&
                b.ClosingDate == product.ClosingDate &&
                b.CategoryId == product.CategoryId &&
                b.UserId == product.UserId
            )), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task ProductService_GetByIdAsync_ReturnsProdyctModel()
        {
            var expected = ExpectedProductsModel.First();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.ProductRepository.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(Products.First);
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var actual = await productService.GetByIdAsync(expected.Id);

            Assert.That(actual, Is.EqualTo(expected).Using(new ProductModelEqualityComparer()));
        }

        [Test]
        public void ProductService_GetRatesByProductId()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(p => p.RateRepository.FindAll()).Returns(Rates.AsQueryable());
            var productService = new ProductService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            var rates = productService.GetRatesByProductId(Products[0].Id);

            Assert.That(rates, Is.EqualTo(ExpectedRatesModel).Using(new RateModelEqualityComparer()));
        }

        private static List<User> Users = new List<User>
        {
            new User {Id ="1", Balance=111, Name = "Oleg" },
            new User {Id ="2", Balance=222, Name = "Ivan" }
        };
        private static List<Category> Categories = new List<Category> 
        { 
            new Category { Id = 1, Name = "phones" },
            new Category {Id = 2, Name = "documents"}
        };
        private static List<Image> Images = new List<Image>
        {
            new Image {Id = 1, ImageName = "1.jpg", ProductId = 1},
            new Image {Id = 2, ImageName = "2.jpg", ProductId = 2},
            new Image {Id = 3, ImageName = "3.jpg", ProductId = 1}
        };
        private static List<Product> Products = new List<Product>
        {
            new Product {Id = 1, Name = "name1", IsAvailable = true, Description = "", InitialPrice = 100, CategoryId = 1, Category = Categories[0], UserId = "1", User = Users[0]},
            new Product {Id = 2, Name = "name2", IsAvailable = true, Description = "", InitialPrice = 110, CategoryId = 2, Category = Categories[1], UserId = "1", User = Users[0]},
            new Product {Id = 3, Name = "name1", IsAvailable = true, Description = "", InitialPrice = 220, CategoryId = 2, Category = Categories[1], UserId = "2", User = Users[1]},
            new Product {Id = 4, Name = "name1", IsAvailable = false, Description = "", InitialPrice = 130, CategoryId = 2, Category = Categories[1], UserId = "1", User = Users[0]}

        };
        private static List<Rate> Rates = new List<Rate>
        {
            new Rate { Id = 1, IsWinning = false, SuggestedPrice = 110, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "1" },
            new Rate { Id = 2, IsWinning = false, SuggestedPrice = 1100, CreatingDate = new DateTime(2020, 08, 30), ProductId = 2, UserId = "1" },
            new Rate { Id = 3, IsWinning = true, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "2" }
            
        };
        private static List<RateModel> ExpectedRatesModel = new List<RateModel>
        {
            new RateModel { Id = 1, IsWinning = false, SuggestedPrice = 110, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "1" },
            new RateModel { Id = 3, IsWinning = true, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "2" }
        };
        private static List<ProductModel> ExpectedProductsModel = new List<ProductModel>
        {
            new ProductModel {Id = 1, Name = "name1", IsAvailable = true, Description = "", InitialPrice = "100", CategoryId = 1, UserId = "1"},
            new ProductModel {Id = 2, Name = "name2", IsAvailable = true, Description = "", InitialPrice = "110", CategoryId = 2, UserId = "1"},
            new ProductModel {Id = 3, Name = "name1", IsAvailable = true, Description = "", InitialPrice = "220", CategoryId = 2, UserId = "2"}
        };
        private static List<ImageModel> ExpectedImages = new List<ImageModel>
        {
            new ImageModel {Id = 1, ImageName = "1.jpg", ProductId = 1},
            new ImageModel {Id = 3, ImageName = "3.jpg", ProductId = 1}
        };
        private static List<ProductModel> ExpectedProductsModelUsingFilter1 = new List<ProductModel>
        {
            new ProductModel {Id = 2, Name = "name2", IsAvailable = true, Description = "", InitialPrice = "110", CategoryId = 2, UserId = "1"}
        };
        private static List<ProductModel> ExpectedProductsModelUsingFilter2 = new List<ProductModel>
        {
            new ProductModel {Id = 2, Name = "name2", IsAvailable = true, Description = "", InitialPrice = "110", CategoryId = 2, UserId = "1"}
        };
        private static List<ProductModel> ExpectedProductsModelUsingFilter3 = new List<ProductModel>
        {
            new ProductModel {Id = 3, Name = "name1", IsAvailable = true, Description = "", InitialPrice = "220", CategoryId = 2, UserId = "2"}
        };
    }
}
