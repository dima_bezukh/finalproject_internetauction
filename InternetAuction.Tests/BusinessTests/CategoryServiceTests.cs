﻿using Moq;
using Business.Interfaces;
using Business.Models;
using Business.Services;
using Business.Validation;
using NUnit.Framework;
using System.Threading.Tasks;
using Data.Interfaces;
using Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace InternetAuction.Tests.BusinessTests
{
    public class CategoryServiceTests
    {
        [Test]
        public async Task CategoryService_AddAsync_AddsModel()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.CategoryRepository.AddAsync(It.IsAny<Category>()));
            var categoryService = new CategoryService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var category = new CategoryModel {Id=100, Name = "phones" };

            await categoryService.AddAsync(category);

            mockUnitOfWork.Verify(x => x.CategoryRepository.AddAsync(It.Is<Category>(b => b.Id == category.Id && b.Name == category.Name)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        [Test]
        public void CategoryService_AddAsync_ThrowsExceptionWithEmptyCategoryName()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.CategoryRepository.AddAsync(It.IsAny<Category>()));
            mockUnitOfWork.Setup(x => x.CategoryRepository.FindAll()).Returns(categories.AsQueryable());
            var categoryService = new CategoryService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var category = new CategoryModel { Id = 100, Name = "" };

            Assert.ThrowsAsync<InternetAuctionException>(() => categoryService.AddAsync(category));
        }
        [Test]
        public void CategoryService_AddAsync_ThrowsExceptionWithExistedCategoryName()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.CategoryRepository.AddAsync(It.IsAny<Category>()));
            mockUnitOfWork.Setup(x => x.CategoryRepository.FindAll()).Returns(categories.AsQueryable());
            var categoryService = new CategoryService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var category = new CategoryModel { Id = 100, Name = "phones" };

            Assert.ThrowsAsync<InternetAuctionException>(() => categoryService.AddAsync(category));
        }
        private List<Category> categories = new List<Category>
        {
            new Category{Id =1, Name = "phones"},
            new Category{Id =1, Name = "laptops"}
        };
    }
}
