﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Business.Models;
using Data.Entities;

namespace InternetAuction.Tests
{
    internal class StatusEqualityComparer : IEqualityComparer<Status>
    {
        public bool Equals([AllowNull] Status x, [AllowNull] Status y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id && x.Name == y.Name;
        }

        public int GetHashCode([DisallowNull] Status obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class CategoryEqualityComparer : IEqualityComparer<Category>
    {
        public bool Equals([AllowNull] Category x, [AllowNull] Category y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id && x.Name == y.Name;
        }

        public int GetHashCode([DisallowNull] Category obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class ProductEqualityComparer : IEqualityComparer<Product>
    {
        public bool Equals([AllowNull] Product x, [AllowNull] Product y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id && x.Name == y.Name
                    && x.Description == y.Description
                    && x.InitialPrice == y.InitialPrice
                    && x.IsAvailable == y.IsAvailable
                    && x.ClosingDate == y.ClosingDate
                    && x.CategoryId == y.CategoryId
                    && x.UserId == y.UserId;
        }

        public int GetHashCode([DisallowNull] Product obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class UserEqualityComparer : IEqualityComparer<User>
    {
        public bool Equals([AllowNull] User x, [AllowNull] User y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id && x.Balance == y.Balance
                    && x.Email == y.Email
                    && x.UserName == y.UserName;
        }

        public int GetHashCode([DisallowNull] User obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class RateEqualityComparer : IEqualityComparer<Rate>
    {
        public bool Equals([AllowNull] Rate x, [AllowNull] Rate y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id && x.IsWinning == y.IsWinning
                    && x.SuggestedPrice == y.SuggestedPrice
                    && x.CreatingDate == y.CreatingDate
                    && x.ProductId == y.ProductId
                    && x.UserId == y.UserId;
        }

        public int GetHashCode([DisallowNull] Rate obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class RateModelEqualityComparer : IEqualityComparer<RateModel>
    {
        public bool Equals([AllowNull] RateModel x, [AllowNull] RateModel y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id && x.IsWinning == y.IsWinning
                    && x.SuggestedPrice == y.SuggestedPrice
                    && x.CreatingDate == y.CreatingDate
                    && x.ProductId == y.ProductId
                    && x.UserId == y.UserId;
        }

        public int GetHashCode([DisallowNull] RateModel obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class OrderEqualityComparer : IEqualityComparer<Order>
    {
        public bool Equals([AllowNull] Order x, [AllowNull] Order y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id && x.CreatingDate == y.CreatingDate
                    && x.ShippingAddress == y.ShippingAddress
                    && x.CreatingDate == y.CreatingDate
                    && x.StatusId == y.StatusId
                    && x.RateId == y.RateId;
        }

        public int GetHashCode([DisallowNull] Order obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class OrderModelEqualityComparer : IEqualityComparer<OrderModel>
    {
        public bool Equals([AllowNull] OrderModel x, [AllowNull] OrderModel y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id && x.CreatingDate == y.CreatingDate
                    && x.ShippingAddress == y.ShippingAddress
                    && x.CreatingDate == y.CreatingDate
                    && x.StatusId == y.StatusId
                    && x.RateId == y.RateId;
        }

        public int GetHashCode([DisallowNull] OrderModel obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class ProductModelEqualityComparer : IEqualityComparer<ProductModel>
    {
        public bool Equals([AllowNull] ProductModel x, [AllowNull] ProductModel y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id && x.Name == y.Name
                    && x.Description == y.Description
                    && x.InitialPrice == y.InitialPrice
                    && x.IsAvailable == y.IsAvailable
                    && x.ClosingDate == y.ClosingDate
                    && x.CategoryId == y.CategoryId
                    && x.UserId == y.UserId;
        }

        public int GetHashCode([DisallowNull] ProductModel obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class ImageModelEqualityComparer : IEqualityComparer<ImageModel>
    {
        public bool Equals([AllowNull] ImageModel x, [AllowNull] ImageModel y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id
                    && x.ImageName == y.ImageName
                    && x.ProductId == y.ProductId;
        }

        public int GetHashCode([DisallowNull] ImageModel obj)
        {
            return obj.GetHashCode();
        }
    }
}
