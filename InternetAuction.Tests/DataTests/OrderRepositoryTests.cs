﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data;
using Data.Entities;
using Data.Repositories;
using NUnit.Framework;
using Microsoft.EntityFrameworkCore;

namespace InternetAuction.Tests.DataTests
{
    [TestFixture]
    public class OrderRepositoryTests
    {
        private DbContextOptions<AuctionDbContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = UnitTestHelper.GetUnitTestDbOptions();
        }
        [Test]
        public async Task OrderRepository_AddAsync()
        {
            using (var context = new AuctionDbContext(_options))
            {
                OrderRepository orderRepository = new OrderRepository(context);
                var rate = new Order { CreatingDate = DateTime.Now, RateId = 2, ShippingAddress = "", StatusId = 1 };
                var expectedCount = context.Orders.Count() + 1;

                await orderRepository.AddAsync(rate);
                await context.SaveChangesAsync();

                Assert.That(context.Orders.Count(), Is.EqualTo(expectedCount));
            }
        }
        [Test]
        public async Task OrderRepository_Delete()
        {
            using (var context = new AuctionDbContext(_options))
            {
                OrderRepository orderRepository = new OrderRepository(context);

                Order order = await context.Orders.FirstOrDefaultAsync();
                var expectedCount = context.Orders.Count() - 1;

                orderRepository.Delete(order);
                await context.SaveChangesAsync();

                Assert.That(context.Orders.Count(), Is.EqualTo(expectedCount));
            }
        }
        [Test]
        public async Task RateRepository_DeleteByIdAsync()
        {
            using (var context = new AuctionDbContext(_options))
            {
                OrderRepository orderRepository = new OrderRepository(context);
                var expectedCount = context.Orders.Count() - 1;

                await orderRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();

                Assert.That(context.Orders.Count(), Is.EqualTo(expectedCount));
            }
        }
        [Test]
        public void RateRepository_FindAll()
        {
            using (var context = new AuctionDbContext(_options))
            {
                OrderRepository orderRepository = new OrderRepository(context);

                var orders = orderRepository.FindAll();

                Assert.That(orders, Is.EqualTo(ExpectedOrders).Using(new OrderEqualityComparer()));
            }
        }
        [Test]
        public void RateRepository_FindAllWithDetails()
        {
            using (var context = new AuctionDbContext(_options))
            {
                OrderRepository orderRepository = new OrderRepository(context);

                var orders = orderRepository.FindAllWithDetails();

                Assert.That(orders, Is.EqualTo(ExpectedOrders).Using(new OrderEqualityComparer()));
                Assert.That(orders.Select(n => n.Status).Distinct().OrderBy(p => p.Id), Is.EqualTo(ExpectedStatuses).Using(new StatusEqualityComparer()));
                Assert.That(orders.Select(n => n.Rate).OrderBy(p => p.Id), Is.EqualTo(ExpectedRates.OrderBy(p => p.Id)).Using(new RateEqualityComparer()));
            }
        }
        [Test]
        public async Task RateRepository_GetByIsAsync()
        {
            using (var context = new AuctionDbContext(_options))
            {
                OrderRepository orderRepository = new OrderRepository(context);

                var order = await orderRepository.GetByIdAsync(1);
                var expected = ExpectedOrders.FirstOrDefault(p => p.Id == 1);

                Assert.That(order, Is.EqualTo(expected).Using(new OrderEqualityComparer()));
            }
        }
        [Test]
        public async Task RateRepository_Update()
        {
            using (var context = new AuctionDbContext(_options))
            {
                OrderRepository orderRepository = new OrderRepository(context);
                var order = new Order { Id = 1, CreatingDate = new DateTime(2020, 08, 31), RateId = 3, ShippingAddress = "newAddress", StatusId = 1 };

                orderRepository.Update(order);
                await context.SaveChangesAsync();
                var expected = await orderRepository.GetByIdAsync(order.Id);

                Assert.That(order, Is.EqualTo(expected).Using(new OrderEqualityComparer()));
            }
        }
        private static IEnumerable<Rate> ExpectedRates =>
            new[]
            {
                new Rate { Id = 3, IsWinning = true, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "12" }
            };
        private static IEnumerable<Order> ExpectedOrders =>
            new[]
            {
                new Order { Id = 1, CreatingDate = new DateTime(2020, 08, 31), RateId = 3, ShippingAddress = "", StatusId = 1 }
            };
        private static IEnumerable<Status> ExpectedStatuses =>
            new[]
            {
                new Status { Id=1, Name = "Created" }
            };
    }
}
