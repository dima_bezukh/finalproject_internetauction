﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data;
using Data.Entities;
using Data.Repositories;
using NUnit.Framework;
using Microsoft.EntityFrameworkCore;

namespace InternetAuction.Tests.DataTests
{
    [TestFixture]
    public class CategoryRepositoryTests
    {
        private DbContextOptions<AuctionDbContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = UnitTestHelper.GetUnitTestDbOptions();
        }

        [Test]
        public async Task CategoryRepository_AddAsync()
        {
            using(var context = new AuctionDbContext(_options))
            {
                CategoryRepository categoryRepository = new CategoryRepository(context);
                var category = new Category { Name = "newCategory" };

                var expectedCount = context.Categories.Count() + 1;
                await categoryRepository.AddAsync(category);
                await context.SaveChangesAsync();

                Assert.That(context.Categories.Count(), Is.EqualTo(expectedCount));
            }
        }

        [Test]
        public async Task CategoryRepository_DeleteByIdAsync()
        {
            using (var context = new AuctionDbContext(_options))
            {
                CategoryRepository categoryRepository = new CategoryRepository(context);

                var expectedCount = context.Categories.Count() - 1;
                await categoryRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();

                Assert.That(context.Categories.Count(), Is.EqualTo(expectedCount));
            }
        }
        [Test]
        public void CategoryRepository_FindAll()
        {
            using (var context = new AuctionDbContext(_options))
            {
                CategoryRepository categoryRepository = new CategoryRepository(context);

                var categories = categoryRepository.FindAll().ToList();

                Assert.That(categories, Is.EqualTo(ExpectedCategories).Using(new CategoryEqualityComparer()));
            }
        }
        [Test]
        public void CategoryRepository_FindAllWithDetails()
        {
            using (var context = new AuctionDbContext(_options))
            {
                CategoryRepository categoryRepository = new CategoryRepository(context);

                var categories = categoryRepository.FindAllWithDetails().ToList();

                Assert.That(categories, Is.EqualTo(ExpectedCategories).Using(new CategoryEqualityComparer()));
                Assert.IsNotNull(categories[0].Products);
            }
        }
        private static IEnumerable<Category> ExpectedCategories =>
            new[]
            {
               new Category
                {
                    Id = 1,
                    Name = "Mobiles"
                },
                new Category
                {
                    Id = 2,
                    Name = "Laptops"
                },
                new Category
                {
                    Id = 3,
                    Name = "TV"
                },
                new Category
                {
                    Id = 4,
                    Name = "Tablets"
                },
                new Category
                {
                    Id = 5,
                    Name = "Smartwatches"
                },
                new Category
                {
                    Id = 6,
                    Name = "Headphones"
                },
                new Category
                {
                    Id = 7,
                    Name = "Cameras"
                }
            };
    }
}
