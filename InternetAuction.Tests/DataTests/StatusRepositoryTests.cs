﻿using System.Linq;
using System.Threading.Tasks;
using Data;
using Data.Entities;
using Data.Repositories;
using NUnit.Framework;

namespace InternetAuction.Tests.DataTests
{
    [TestFixture]
    public class StatusRepositoryTests
    {
        [Test]
        public async Task StatusRepository_AddAsync()
        {
            await using var context = new AuctionDbContext(UnitTestHelper.GetUnitTestDbOptions());

            StatustRepository statustRepository = new StatustRepository(context);
            var status = new Status { Name = "newStatus" };

            var expectedCount = context.Statuses.Count() + 1;
            await statustRepository.AddAsync(status);
            await context.SaveChangesAsync();

            Assert.That(context.Statuses.Count(), Is.EqualTo(expectedCount));
        }

        [Test]
        public async Task StatusRepository_DeleteByIdAsync()
        {
            await using var context = new AuctionDbContext(UnitTestHelper.GetUnitTestDbOptions());

            StatustRepository statustRepository = new StatustRepository(context);

            var expectedCount = context.Statuses.Count() - 1;
            await statustRepository.DeleteByIdAsync(1);
            await context.SaveChangesAsync();

            Assert.That(context.Statuses.Count(), Is.EqualTo(expectedCount));
        }
    }
}
