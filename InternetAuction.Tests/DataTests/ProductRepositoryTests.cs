﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data;
using Data.Entities;
using Data.Repositories;
using NUnit.Framework;
using Microsoft.EntityFrameworkCore;

namespace InternetAuction.Tests.DataTests
{
    [TestFixture]
    public class ProductRepositoryTests
    {
        private DbContextOptions<AuctionDbContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = UnitTestHelper.GetUnitTestDbOptions();
        }

        [Test]
        public async Task ProductRepository_AddAsync()
        {
            using (var context = new AuctionDbContext(_options))
            {
                ProductRepository productRepository = new ProductRepository(context);
                var product = new Product { Name = "TeslaPhone", InitialPrice = 500, Description = "", IsAvailable = true, ClosingDate = DateTime.Now.AddHours(1), CategoryId = 1, UserId = "123" };
                var expectedCount = context.Products.Count() + 1;

                await productRepository.AddAsync(product);
                await context.SaveChangesAsync();

                Assert.That(context.Products.Count(), Is.EqualTo(expectedCount));
            }
        }

        [Test]
        public async Task ProductRepository_Delete()
        {
            using (var context = new AuctionDbContext(_options))
            {
                ProductRepository productRepository = new ProductRepository(context);

                Product product = await context.Products.FirstOrDefaultAsync();
                var expectedCount = context.Products.Count() - 1;

                productRepository.Delete(product);
                await context.SaveChangesAsync();

                Assert.That(context.Products.Count(), Is.EqualTo(expectedCount));
            }
        }

        [Test]
        public async Task ProductRepository_DeleteByIdAsync()
        {
            using (var context = new AuctionDbContext(_options))
            {
                ProductRepository productRepository = new ProductRepository(context);
                var expectedCount = context.Products.Count() - 1;

                await productRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();

                Assert.That(context.Products.Count(), Is.EqualTo(expectedCount));
            }
        }

        [Test]
        public void ProductRepository_FindAll()
        {
            using (var context = new AuctionDbContext(_options))
            {
                ProductRepository productRepository = new ProductRepository(context);

                var products = productRepository.FindAll();

                Assert.That(products, Is.EqualTo(ExpectedProducts).Using(new ProductEqualityComparer()));
            }
        }

        [Test]
        public void ProductRepository_FindAllWithDetails()
        {
            using (var context = new AuctionDbContext(_options))
            {
                ProductRepository productRepository = new ProductRepository(context);

                var products = productRepository.FindAllWithDetails().ToList();

                Assert.That(products, Is.EqualTo(ExpectedProducts).Using(new ProductEqualityComparer()));
                Assert.That(products.Select(n => n.Category).OrderBy(p => p.Id), Is.EqualTo(ExpectedCategories).Using(new CategoryEqualityComparer()));
                Assert.That(products.Select(n => n.User).OrderBy(p => p.Id), Is.EqualTo(ExpectedUsers).Using(new UserEqualityComparer()));
                Assert.IsNotNull(products.FirstOrDefault()?.Images);
                Assert.IsNotNull(products.FirstOrDefault()?.Rates);
            }
        }

        [TestCase(1)]
        [TestCase(2)]
        public async Task ProductRepository_GetByIsAsync(int id)
        {
            using (var context = new AuctionDbContext(_options))
            {
                ProductRepository productRepository = new ProductRepository(context);

                var product = await productRepository.GetByIdAsync(id);
                var expected = ExpectedProducts.FirstOrDefault(p => p.Id == id);

                Assert.That(product, Is.EqualTo(expected).Using(new ProductEqualityComparer()));
            }
        }

        [Test]
        public async Task ProductRepository_Update()
        {
            using (var context = new AuctionDbContext(_options))
            {
                ProductRepository productRepository = new ProductRepository(context);
                var product = new Product { Id = 1, Name = "SamsungV2", InitialPrice = 200, Description = "", IsAvailable = true, ClosingDate = new DateTime(2020, 12, 17), CategoryId = 1, UserId = "123" };
                
                productRepository.Update(product);
                await context.SaveChangesAsync();
                var expected = await productRepository.GetByIdAsync(product.Id);

                Assert.That(product, Is.EqualTo(expected).Using(new ProductEqualityComparer()));
            }
        }
        private static IEnumerable<Product> ExpectedProducts =>
            new[]
            {
                new Product { Id = 1, Name = "Samsung", InitialPrice = 100, Description = "", IsAvailable = true, ClosingDate = new DateTime(2020, 12, 7), CategoryId = 1, UserId="123" },
                new Product { Id = 2, Name = "Asus", InitialPrice = 1000, Description = "", IsAvailable = true, ClosingDate = new DateTime(2020, 12, 7), CategoryId = 2, UserId = "1234" }
            };
        private static IEnumerable<Category> ExpectedCategories =>
            new[]
            {
                new Category { Id = 1, Name = "Mobiles" },
                new Category { Id = 2, Name = "Laptops" }
            };
        private static IEnumerable<User> ExpectedUsers =>
            new[]
            {
                new User { Id="123", Balance = 1000, Email = "oleg@gmail.com", Name = "Oleg" },
                new User { Id = "1234", Balance = 1000, Email = "inna@gmail.com", Name = "Inna" }
            };
    }
}
