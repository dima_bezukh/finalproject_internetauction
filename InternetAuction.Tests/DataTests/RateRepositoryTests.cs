﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Data;
using Data.Entities;
using Data.Repositories;
using NUnit.Framework;
using Microsoft.EntityFrameworkCore;

namespace InternetAuction.Tests.DataTests
{
    [TestFixture]
    public class RateRepositoryTests
    {
        private DbContextOptions<AuctionDbContext> _options;

        [SetUp]
        public void Setup()
        {
            _options = UnitTestHelper.GetUnitTestDbOptions();
        }
        [Test]
        public async Task RateRepository_AddAsync()
        {
            using (var context = new AuctionDbContext(_options))
            {
                RateRepository rateRepository = new RateRepository(context);
                var rate = new Rate { IsWinning = false, ProductId = 1, UserId = "12", SuggestedPrice = 200, CreatingDate = DateTime.Now };
                var expectedCount = context.Rates.Count() + 1;

                await rateRepository.AddAsync(rate);
                await context.SaveChangesAsync();

                Assert.That(context.Rates.Count(), Is.EqualTo(expectedCount));
            }
        }
        [Test]
        public async Task RateRepository_Delete()
        {
            using (var context = new AuctionDbContext(_options))
            {
                RateRepository rateRepository = new RateRepository(context);

                Rate rate = await context.Rates.FirstOrDefaultAsync();
                var expectedCount = context.Rates.Count() - 1;

                rateRepository.Delete(rate);
                await context.SaveChangesAsync();

                Assert.That(context.Rates.Count(), Is.EqualTo(expectedCount));
            }
        }
        [Test]
        public async Task RateRepository_DeleteByIdAsync()
        {
            using (var context = new AuctionDbContext(_options))
            {
                RateRepository rateRepository = new RateRepository(context);
                var expectedCount = context.Rates.Count() - 1;

                await rateRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();

                Assert.That(context.Rates.Count(), Is.EqualTo(expectedCount));
            }
        }
        [Test]
        public void RateRepository_FindAll()
        {
            using (var context = new AuctionDbContext(_options))
            {
                RateRepository rateRepository = new RateRepository(context);

                var rates = rateRepository.FindAll();

                Assert.That(rates, Is.EqualTo(ExpectedRates).Using(new RateEqualityComparer()));
            }
        }
        [Test]
        public void RateRepository_FindAllWithDetails()
        {
            using (var context = new AuctionDbContext(_options))
            {
                RateRepository rateRepository = new RateRepository(context);

                var rates = rateRepository.FindAllWithDetails().ToList();

                Assert.That(rates, Is.EqualTo(ExpectedRates).Using(new RateEqualityComparer()));
                Assert.That(rates.Select(n => n.Product).Distinct().OrderBy(p => p.Id), Is.EqualTo(ExpectedProducts).Using(new ProductEqualityComparer()));
                Assert.That(rates.Select(n => n.User).Distinct().OrderBy(p => p.Id), Is.EqualTo(ExpectedUsers.OrderBy(p=>p.Id)).Using(new UserEqualityComparer()));
                Assert.That(rates.Where(p=>p.Order!=null).Select(n => n.Order).OrderBy(p => p.Id), Is.EqualTo(ExpectedOrders).Using(new OrderEqualityComparer()));
            }
        }
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task RateRepository_GetByIsAsync(int id)
        {
            using (var context = new AuctionDbContext(_options))
            {
                RateRepository rateRepository = new RateRepository(context);

                var rate = await rateRepository.GetByIdAsync(id);
                var expected = ExpectedRates.FirstOrDefault(p => p.Id == id);

                Assert.That(rate, Is.EqualTo(expected).Using(new RateEqualityComparer()));
            }
        }
        [Test]
        public async Task RateRepository_Update()
        {
            using (var context = new AuctionDbContext(_options))
            {
                RateRepository rateRepository = new RateRepository(context);
                var rate = new Rate { Id = 1, IsWinning = false, SuggestedPrice = 130, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "1234" };
                
                rateRepository.Update(rate);
                await context.SaveChangesAsync();
                var expected = await rateRepository.GetByIdAsync(rate.Id);

                Assert.That(rate, Is.EqualTo(expected).Using(new RateEqualityComparer()));
            }
        }
        private static IEnumerable<Rate> ExpectedRates =>
            new[]
            {
                new Rate { Id = 1, IsWinning = false, SuggestedPrice = 110, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "1234" },
                new Rate { Id = 2, IsWinning = false, SuggestedPrice = 1100, CreatingDate = new DateTime(2020, 08, 30), ProductId = 2, UserId = "123" },
                new Rate { Id = 3, IsWinning = true, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "12" }
            };
        private static IEnumerable<Product> ExpectedProducts =>
            new[]
            {
                new Product { Id = 1, Name = "Samsung", InitialPrice = 100, Description = "", IsAvailable = true, ClosingDate = new DateTime(2020, 12, 7), CategoryId = 1, UserId="123" },
                new Product { Id = 2, Name = "Asus", InitialPrice = 1000, Description = "", IsAvailable = true, ClosingDate = new DateTime(2020, 12, 7), CategoryId = 2, UserId = "1234" }
            };
        private static IEnumerable<User> ExpectedUsers =>
            new[]
            {
                new User { Id="123", Balance = 1000, Email = "oleg@gmail.com", Name = "Oleg" },
                new User { Id = "1234", Balance = 1000, Email = "inna@gmail.com", Name = "Inna" },
                new User { Id = "12", Balance = 1000, Email = "ivan@gmail.com", Name = "Ivan" }
            };
        private static IEnumerable<Order> ExpectedOrders =>
            new[]
            {
                new Order { Id = 1, CreatingDate = new DateTime(2020, 08, 31), RateId = 3, ShippingAddress = "", StatusId = 1 }
            };
    }
}
