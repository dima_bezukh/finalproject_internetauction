﻿using System;
using AutoMapper;
using Business;
using Data;
using Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace InternetAuction.Tests
{
    internal static class UnitTestHelper
    {
        public static DbContextOptions<AuctionDbContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<AuctionDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new AuctionDbContext(options))
            {
                SeedData(context);
            }
            return options;
        }
        public static void SeedData(AuctionDbContext context)
        {
            context.Users.Add(new User { Id="123", Balance = 1000, Email = "oleg@gmail.com", Name = "Oleg" });
            context.Users.Add(new User { Id = "1234", Balance = 1000, Email = "inna@gmail.com", Name = "Inna" });
            context.Users.Add(new User { Id = "12", Balance = 1000, Email = "ivan@gmail.com", Name = "Ivan" });
            context.Products.Add(new Product { Id = 1, Name = "Samsung", InitialPrice = 100, Description = "", IsAvailable = true, ClosingDate = new DateTime(2020, 12, 7), CategoryId = 1, UserId="123" });
            context.Products.Add(new Product { Id = 2, Name = "Asus", InitialPrice = 1000, Description = "", IsAvailable = true, ClosingDate = new DateTime(2020, 12, 7), CategoryId = 2, UserId = "1234" });
            context.Images.Add(new Image { Id = 1, ImageName = "1.jpg", ProductId = 1 });
            context.Images.Add(new Image { Id = 2, ImageName = "2.jpg", ProductId = 1 });
            context.Images.Add(new Image { Id = 3, ImageName = "3.jpg", ProductId = 2 });
            context.Rates.Add(new Rate { Id = 1, IsWinning = false, SuggestedPrice = 110, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "1234" });
            context.Rates.Add(new Rate { Id = 2, IsWinning = false, SuggestedPrice = 1100, CreatingDate = new DateTime(2020, 08, 30), ProductId = 2, UserId = "123" });
            context.Rates.Add(new Rate { Id = 3, IsWinning = true, SuggestedPrice = 120, CreatingDate = new DateTime(2020, 08, 30), ProductId = 1, UserId = "12" });
            context.Orders.Add(new Order { Id = 1, CreatingDate = new DateTime(2020, 08, 31), RateId = 3, ShippingAddress = "", StatusId = 1 });
            context.SaveChanges();
        }
        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }
    }
}
