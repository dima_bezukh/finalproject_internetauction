﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class FilterSearchViewModel
    {
        public string ProductName { get; set; }
        public decimal MaxInitialPrice { get; set; }
        public decimal MinInitialPrice { get; set; }
        public string Seller { get; set; }
        public string CategoryName { get; set; }
    }
}
