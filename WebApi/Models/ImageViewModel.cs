﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class ImageViewModel
    {
        public int Id { get; set; }
        public string ImageName { get; set; }
        public int ProductId { get; set; }
    }
}
