﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class ProductCreateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string InitialPrice { get; set; }
        public DateTime ClosingDate { get; set; }
        public IFormFile File { get; set; }
        public int CategoryId { get; set; }
        public string UserId { get; set; }
    }
}
