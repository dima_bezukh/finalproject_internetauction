﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class RateViewModel
    {
        public int Id { get; set; }
        public bool IsWinning { get; set; }
        public decimal SuggestedPrice { get; set; }
        public DateTime CreatingDate { get; set; }
        public int ProductId { get; set; }
        public string UserId { get; set; }
    }
}
