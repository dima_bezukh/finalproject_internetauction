﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class OrderViewModel
    {
        public int Id { get; set; }
        public DateTime CreatingDate { get; set; }
        public string ShippingAddress { get; set; }
        public int StatusId { get; set; }
        public int RateId { get; set; }
    }
}
