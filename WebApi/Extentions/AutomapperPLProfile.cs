﻿using AutoMapper;
using Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Extentions
{
    public class AutomapperPLProfile : Profile
    {
        public AutomapperPLProfile()
        {
            CreateMap<RegisterViewModel, RegisterModel>();
            CreateMap<UserModel, UserViewModel>();
            CreateMap<LoginViewModel, LoginModel>();
            CreateMap<PaymentDetailViewModel, PaymentDetailModel>();
            CreateMap<ProductCreateModel, ProductModel>();
            CreateMap<ProductModel, ProductViewModel>();
            CreateMap<ExtendedProductModel, ProductViewModel>();
            CreateMap<CategoryViewModel, CategoryModel>().ReverseMap();
            CreateMap<ImageModel, ImageViewModel>().ReverseMap();
            CreateMap<RateModel, RateViewModel>().ReverseMap();
            CreateMap<FilterSearchViewModel, FilterSearchModel>();
            CreateMap<OrderModel, OrderViewModel>().ReverseMap();
        }
    }
}
