import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormBuilder, FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './account/login/login.component';
import { AccountComponent } from './account/account.component';
import { RegistrationComponent } from './account/registration/registration.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RechargeBalanceComponent } from './account/recharge-balance/recharge-balance.component';
import { ProductListComponent } from './product-list/product-list.component';
import { UsersProductListComponent } from './product-list/users-product-list/users-product-list.component';
import { CreateProductComponent } from './product-list/users-product-list/create-product/create-product.component';
import { SearchComponent } from './product-list/search/search.component';
import { ProductDetailsComponent } from './product-list/product-details/product-details.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RateListComponent } from './rate-list/rate-list.component';
import { CreateRateComponent } from './rate-list/create-rate/create-rate.component';
import { OrderListComponent } from './order-list/order-list.component';
import { WonProductsComponent } from './order-list/won-products/won-products.component';
import { CreateOrderComponent } from './order-list/create-order/create-order.component';
import {AuthGuard} from './auth.guard'

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    AccountComponent,
    LoginComponent,
    RegistrationComponent,
    RechargeBalanceComponent,
    ProductListComponent,
    UsersProductListComponent,
    CreateProductComponent,
    SearchComponent,
    ProductDetailsComponent,
    NotFoundComponent,
    RateListComponent,
    CreateRateComponent,
    OrderListComponent,
    WonProductsComponent,
    CreateOrderComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: ProductListComponent, pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegistrationComponent },
      { path: 'account', component: AccountComponent, canActivate: [AuthGuard] },
      { path: 'usersProducts', component: UsersProductListComponent, canActivate: [AuthGuard] },
      { path: 'products', component: ProductListComponent },
      { path: 'products/:id', component: ProductDetailsComponent },
      { path: '**', component: NotFoundComponent }
    ])
  ],
  providers: [AccountComponent, FormBuilder, ProductDetailsComponent, RateListComponent, NavMenuComponent, OrderListComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
