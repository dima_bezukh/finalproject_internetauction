import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Rate } from '../Models/rate.model';

@Injectable({
  providedIn: 'root'
})
export class RateService {

  constructor(private httpClient: HttpClient) { }

  postRate(rate: Rate){
    return this.httpClient.post('/api/Rates', rate);
  }

  deleteRate(id: number){
    return this.httpClient.delete(`/api/Rates/${id}`);
  }

  getRate(id: number){
    return this.httpClient.get<Rate>(`/api/Rates/${id}`);
  }

  getwinningRate(id: number){
    return this.httpClient.get<Rate>(`/api/Products/${id}/GetWinnigRate`);
  }
}
