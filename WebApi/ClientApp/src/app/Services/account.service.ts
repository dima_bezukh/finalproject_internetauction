import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Component, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Paymentdetail } from '../Models/paymentdetail.model';
import { Registration } from '../Models/registration.model';
import { User } from '../Models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private httpClient: HttpClient) { }

  login(email: string, password: string){
    var body = {
      Email: email,
      Password: password
    };
    return this.httpClient.post<User>('/api/Account/LogIn', body);
  }
  logout(){
    return this.httpClient.post('/api/Account/LogOut', {});
  }

  register(registerModel: Registration){
    return this.httpClient.post('/api/Account/Register', registerModel);
  }

  rechargeBalance(paymentDeatil: Paymentdetail){
    return this.httpClient.put<number>('/api/Account/RechargeBalance', paymentDeatil);
  }

  getUserName(id: string){
    return this.httpClient.get<string>(`/api/Account/GetName/${id}`);
  }

  getuser(id: string){
    return this.httpClient.get<User>(`/api/Account/${id}`);
  }
}
