import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from '../Models/category.model';
import { Observable } from 'rxjs';
import { Product } from '../Models/product.model';
import { ProductView } from '../Models/product-view.model';
import { Search } from '../Models/search.model';
import { Rate } from '../Models/rate.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient) { }

  getAllCategories(){
    return this.httpClient.get<Category[]>('/api/Categories');
  }

  createProduct(product: FormData){
    return this.httpClient.post('/api/Products', product);
  }

  getUsersProducts(userId: string){
    return this.httpClient.get<ProductView[]>(`/api/Products/GetUsersProducts/${userId}`);
  }

  getAllProducts() {
    return this.httpClient.get<ProductView[]>('/api/Products/GetAll');
  }

  getByFilter(filter: Search){
    return this.httpClient.post<ProductView[]>('/api/Products/GetAllByFilter', filter);
  }

  deleteUsersProduct(productId: number, ownerId: string){
    return this.httpClient.delete(`/api/Products/DeleteProduct/${productId}/Owner/${ownerId}`);
  }

  getProductById(id: number){
    return this.httpClient.get<ProductView>(`/api/Products/${id}`);
  }

  getAllProductRates(id: number) {
    return this.httpClient.get<Rate[]>(`/api/Products/${id}/GetAllRates`);
  }

  getWonProducts(userid: string){
    return this.httpClient.get<ProductView[]>(`/api/Products/GetWonProducts/${userid}`);
  }
}
