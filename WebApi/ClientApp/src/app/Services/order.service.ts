import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order } from '../Models/order.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private httpClient: HttpClient) { }

  getAllByUserId(id: string){
    return this.httpClient.get<Order[]>(`/api/Account/${id}/GetOrders`);
  }

  getStatus(id: number){
    return this.httpClient.get<string>(`/api/Statuses/${id}`);
  }

  postorder(order: Order){
    return this.httpClient.post(`/api/Orders`, order);
  }
}
