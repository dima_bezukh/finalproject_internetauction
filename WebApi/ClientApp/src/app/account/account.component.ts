import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { User } from '../Models/user.model';
import { OrderListComponent } from '../order-list/order-list.component';
import { AccountService } from '../Services/account.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  public user: User;
  public isAuthenticated: boolean;
  public isAdmin: boolean = false;

  constructor(private accountService: AccountService, private router: Router) { }

  ngOnInit() {
    this.getUser();
  }
  getUser(){
    const userId = localStorage.getItem("key");
    if(userId){
      this.accountService.getuser(userId).subscribe(
        res => {
          this.user = res;
          this.isAuthenticated = true;
          if(this.user.rolesName.some(x=>x==="admin")){
            this.isAdmin = true;
          }
        },
        err => {
          console.log(err.error);
        }
      )
    }
  }

  logout(){
    this.accountService.logout().subscribe(
      res => {
        this.user = undefined;
      }
    );
    localStorage.removeItem("key");
    this.isAuthenticated = false;
    this.router.navigate(['']);
  }

}
