import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Registration } from 'src/app/Models/registration.model';
import { AccountService } from 'src/app/Services/account.service';
import { AccountComponent } from '../account.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registerForm: FormGroup;
  registerModel: Registration = new Registration();
  public error: string;
  constructor(private formBuilder: FormBuilder, private accountService: AccountService,
              private account: AccountComponent, private router: Router,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      UserName: ['', Validators.required],
      Email: ['', [Validators.required, Validators.email]],
      Passwords: this.formBuilder.group({
        Password: ['', [Validators.required]],
        ConfirmPassword: ['', Validators.required]
      }, { validator: this.comparePasswords })
    });
  }
  comparePasswords(fb: FormGroup) {
    let confirmPswrdCtrl = fb.get('ConfirmPassword');
    if (confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl.errors) {
      if (fb.get('Password').value != confirmPswrdCtrl.value)
        confirmPswrdCtrl.setErrors({ passwordMismatch: true });
      else
        confirmPswrdCtrl.setErrors(null);
    }
  }
  onSubmit(){
    this.registerModel.UserName = this.registerForm.value.UserName;
    this.registerModel.Email = this.registerForm.value.Email;
    this.registerModel.Password = this.registerForm.value.Passwords.Password,
    this.registerModel.PasswordConfirm = this.registerForm.value.Passwords.ConfirmPassword;
    this.accountService.register(this.registerModel).subscribe(
      res => {
        this.error = '';
        this.toastr.success('Submitted successfully', 'Account registred');
        this.router.navigate(['login']);
      }, err => {
        this.error = err.error;
      }
    );
  }
}
