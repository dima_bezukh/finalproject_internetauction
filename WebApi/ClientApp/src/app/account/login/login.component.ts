import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/Services/account.service';
import { AccountComponent } from '../account.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  public error: string;
  constructor(private accountService: AccountService, private account: AccountComponent, private router: Router,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.error = '';
    this.loginForm = this.formBuilder.group({
      Email: ['', [Validators.required, Validators.email]],
      Password: ['', Validators.required]
    });
  }
  login(){
    this.accountService.login(this.loginForm.value.Email, this.loginForm.value.Password).subscribe(
      res => {
        this.error = '';
        localStorage.setItem("key", res.id);
        this.account.getUser();
        this.router.navigate(['/']);
      }, err => {
        this.error = err.error;
      }
    )
  }
}
