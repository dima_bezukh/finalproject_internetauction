import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Paymentdetail } from 'src/app/Models/paymentdetail.model';
import { NavMenuComponent } from 'src/app/nav-menu/nav-menu.component';
import { AccountService } from 'src/app/Services/account.service';
import { AccountComponent } from '../account.component';

@Component({
  selector: 'app-recharge-balance',
  templateUrl: './recharge-balance.component.html',
  styleUrls: ['./recharge-balance.component.css']
})
export class RechargeBalanceComponent implements OnInit {

  paymentForm: FormGroup;
  public error: string;
  paymentDetail: Paymentdetail = new Paymentdetail();

  constructor(private formBuilder: FormBuilder, private accountService: AccountService,
              private account: AccountComponent, private router: Router,
              private toastr: ToastrService, private navmenu: NavMenuComponent) { 
      navmenu.account.getUser();
  }

  ngOnInit() {
    this.paymentForm = this.formBuilder.group({
      CardNumber: ['', Validators.required],
      ExpirationDate: ['', Validators.required],
      SecurityCode: ['', Validators.required],
      AmountOfMoney: ['', Validators.required]
    });
  }
  onSubmit(){
    this.paymentDetail.UserId = this.account.user.id;
    this.paymentDetail.CardNumber = this.paymentForm.value.CardNumber;
    this.paymentDetail.SecurityCode = this.paymentForm.value.SecurityCode;
    this.paymentDetail.ExpirationDate = this.paymentForm.value.ExpirationDate;
    this.paymentDetail.AmountOfMoney = this.paymentForm.value.AmountOfMoney;
    this.accountService.rechargeBalance(this.paymentDetail).subscribe(
      res => {
        this.error = '';
        this.navmenu.account.user.balance+= res;
        this.navmenu.account.getUser();
        this.toastr.success('Submitted successfully', 'Your balance was recharged');
        this.paymentForm.reset();
      },
      err => {
        this.error = err.error;
      }
    )
  }
}
