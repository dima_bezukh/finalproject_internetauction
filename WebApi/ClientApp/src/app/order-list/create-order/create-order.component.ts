import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AccountComponent } from 'src/app/account/account.component';
import { Order } from 'src/app/Models/order.model';
import { NavMenuComponent } from 'src/app/nav-menu/nav-menu.component';
import { OrderService } from 'src/app/Services/order.service';
import { WonProductsComponent } from '../won-products/won-products.component';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.css']
})
export class CreateOrderComponent implements OnInit {

  @Input() rateId: number;
  order: Order = new Order();
  orderForm: FormGroup;

  constructor(private orderService: OrderService, private account: AccountComponent,
    private toastr: ToastrService, private formBuilder: FormBuilder, private wonProducts: WonProductsComponent,
    private navmenu: NavMenuComponent) {
      account.getUser();
     }

  ngOnInit() {
    this.orderForm = this.formBuilder.group({
      ShippingAddress: ['', Validators.required]
    })
  }

  onSubmit(){
    this.order.shippingAddress = this.orderForm.value.ShippingAddress;
    this.order.rateId = this.rateId;

    this.orderService.postorder(this.order).subscribe(
      res => {
        this.toastr.success('Submitted successfully', "Order was created. Refresh order list, to see your new order");
        this.wonProducts.refreshWonProductList();
        this.navmenu.account.getUser();
      },
      err => {
        this.toastr.error('Submitted failed', err.error);
      }
    )
    
  }

}
