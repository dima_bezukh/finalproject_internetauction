import { Component, OnInit } from '@angular/core';
import { ProductView } from 'src/app/Models/product-view.model';
import { Rate } from 'src/app/Models/rate.model';
import { WonProduct } from 'src/app/Models/won-product.model';
import { ProductService } from 'src/app/Services/product.service';
import { RateService } from 'src/app/Services/rate.service';

@Component({
  selector: 'app-won-products',
  templateUrl: './won-products.component.html',
  styleUrls: ['./won-products.component.css']
})
export class WonProductsComponent implements OnInit {

  wonProducts: WonProduct[];
  error: string;
  constructor(private productService: ProductService, private rateService: RateService) { }

  ngOnInit() {
    this.refreshWonProductList();
  }

  refreshWonProductList(){
    const userId = localStorage.getItem("key");
    this.productService.getWonProducts(userId).subscribe(
      res => {
        this.wonProducts = []
        for(let prod of res){
          var wonProd = new WonProduct();
          wonProd.product = prod
          this.wonProducts.push(wonProd)
        }
        for(let item of this.wonProducts){
          this.rateService.getwinningRate(item.product.id).subscribe(
            res => {
              item.rate = res
            }
          );
        }
        
      },
      err => {
        this.error = err.error;
        this.wonProducts = undefined;
      }
    )
  }
}
