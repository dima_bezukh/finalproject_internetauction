import { Component, OnInit } from '@angular/core';
import { Order } from '../Models/order.model';
import { OrderService } from '../Services/order.service';
import { ProductService } from '../Services/product.service';
import { RateService } from '../Services/rate.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {

  orders: Order[];
  error: string;

  constructor(private orderService: OrderService, private productService: ProductService,
      private rateService: RateService) { }

  ngOnInit() {
    this.refreshOrders();
  }

  refreshOrders(){
    const userId = localStorage.getItem("key");
    this.orderService.getAllByUserId(userId).subscribe(
      res => {
        this.orders = res;
        this.error = ''
        for(let order of this.orders){
          this.orderService.getStatus(order.statusId).subscribe(
            res => {
              order.statusName = res;
            }
          )
          this.rateService.getRate(order.rateId).subscribe(
            res => {
              order.rate = res;
              this.productService.getProductById(res.productId).subscribe(
                res => {
                  order.product = res;
                }
              )
            }
          )
        }
      },
      err => {
        this.error = err.error;
      }
    )
  }


}
