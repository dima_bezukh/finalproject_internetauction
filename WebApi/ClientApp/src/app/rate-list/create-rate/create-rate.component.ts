import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AccountComponent } from 'src/app/account/account.component';
import { Rate } from 'src/app/Models/rate.model';
import { ProductDetailsComponent } from 'src/app/product-list/product-details/product-details.component';
import { RateService } from 'src/app/Services/rate.service';
import { RateListComponent } from '../rate-list.component';

@Component({
  selector: 'app-create-rate',
  templateUrl: './create-rate.component.html',
  styleUrls: ['./create-rate.component.css']
})
export class CreateRateComponent implements OnInit {

  @Input()
  productId: number;
  rateForm: FormGroup;
  rate: Rate = new Rate();

  constructor(private rateservice: RateService, private account: AccountComponent,
    private toastr: ToastrService, public productDetails: ProductDetailsComponent,
    private formBuilder: FormBuilder) {
    account.getUser();
   }

  ngOnInit() {
    this.rateForm = this.formBuilder.group({
      SuggestedPrice: [this.productDetails.availablePrice, Validators.required]
    })
  }

  

  onSubmit(){
    this.rate.productId = this.productId;
    this.rate.userId = this.account.user.id;
    this.rate.suggestedPrice = this.rateForm.value.SuggestedPrice;

    this.rateservice.postRate(this.rate).subscribe(
      res => {
        this.productDetails.refreshList();
        this.rateForm.reset();
        this.toastr.success('Submitted successfully', "Rate was created");
      },
      err => {
        this.toastr.error('Submitted failed', `${err.error}`);
      }
    )
  }
}
