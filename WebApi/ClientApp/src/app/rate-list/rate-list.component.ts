import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from 'events';
import { ToastrService } from 'ngx-toastr';
import { AccountComponent } from '../account/account.component';
import { ProductView } from '../Models/product-view.model';
import { Product } from '../Models/product.model';
import { Rate } from '../Models/rate.model';
import { ProductDetailsComponent } from '../product-list/product-details/product-details.component';
import { AccountService } from '../Services/account.service';
import { ProductService } from '../Services/product.service';
import { RateService } from '../Services/rate.service';

@Component({
  selector: 'app-rate-list',
  templateUrl: './rate-list.component.html',
  styleUrls: ['./rate-list.component.css']
})
export class RateListComponent implements OnInit {

  error: string;

  constructor(private account: AccountComponent, private productService: ProductService,
    private accountService: AccountService, private productDetail: ProductDetailsComponent,
    private rateService: RateService, private toastr: ToastrService) {
      this.account.getUser();
     }

  ngOnInit() {
    this.productDetail.refreshList();
  }

  onDelete(id: number){
    if (confirm('Are you sure to delete your rate?')){
        this.rateService.deleteRate(id).subscribe(
        res => {
          this.productDetail.refreshList();
          this.toastr.info("Deleted successfully", "Rate was deleted");
        },
        err => {
          this.error = err.error;
        }
      )
    }
  }
}
