export class Paymentdetail {
    UserId: string;
    CardNumber: string;
    ExpirationDate: string;
    SecurityCode: string;
    AmountOfMoney: string;
}
