import { ProductView } from "./product-view.model";
import { Rate } from "./rate.model";

export class WonProduct {
    product: ProductView;
    rate: Rate;
}
