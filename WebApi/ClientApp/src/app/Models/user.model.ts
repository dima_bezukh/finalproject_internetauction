export class User {
    id: string;
    name: string;
    email: string;
    balance: number;
    rolesName: string[];
}
