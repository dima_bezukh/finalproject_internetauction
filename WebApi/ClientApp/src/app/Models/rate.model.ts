export class Rate {
    id: number;
    isWinning: boolean;
    suggestedPrice: number;
    creatingDate: Date;
    productId: number;
    userId: string;
    userName: string;
}
