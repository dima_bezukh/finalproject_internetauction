export class Product {
    id: number;
    name: string;
    description: string;
    initialPrice: string;
    closingDate: Date;
    file: File;
    categoryId: number;
    userId: string;
}
