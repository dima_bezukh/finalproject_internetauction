import { ProductView } from "./product-view.model";
import { Rate } from "./rate.model";

export class Order {
    id: number;
    creatingDate: Date;
    shippingAddress: string;
    statusId: number;
    rateId: number;
    product: ProductView;
    statusName: string;
    rate: Rate;
}
