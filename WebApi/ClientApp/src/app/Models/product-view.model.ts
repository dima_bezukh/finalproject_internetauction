export class ProductView {
    id: number;
    name: string;
    description: string;
    initialPrice: string;
    closingDate: Date;
    imagesNames: string[];
    categoryId: number;
    userId: string;
    categoryName: string;
}
