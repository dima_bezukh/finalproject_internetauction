export class Search {
    productName: string;
    maxInitialPrice: number = 0;
    minInitialPrice: number = 0;
    seller: string;
    categoryName: string;
}
