import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductView } from 'src/app/Models/product-view.model';
import { Rate } from 'src/app/Models/rate.model';
import { RateListComponent } from 'src/app/rate-list/rate-list.component';
import { AccountService } from 'src/app/Services/account.service';
import { ProductService } from 'src/app/Services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  public product: ProductView;
  public ownerName: string;
  public rates: Rate[];
  availablePrice: number;
  public errorMessageRates: string;

  constructor(private productService: ProductService, private router: ActivatedRoute,
    private accountService: AccountService) { }

  ngOnInit() {
    const id = +this.router.snapshot.paramMap.get('id');
    this.productService.getProductById(id).subscribe(
      res => {
        this.product = res;
        this.accountService.getUserName(this.product.userId).subscribe(
          res => {
            this.ownerName = res;
          },
          err => {
            console.log(err.error);
          }
        )
      },
      err => {
        console.log(err.error);
      }
    )
  }

  isAvaliable(){
    return new Date(this.product.closingDate) > new Date();
  }

  refreshList(){
    this.productService.getAllProductRates(this.product.id).subscribe(
      res => {
        this.errorMessageRates = ''
        this.rates = res.sort().reverse();
        for(let rate of this.rates)
        {
          this.accountService.getUserName(rate.userId).subscribe(
            res => {
              rate.userName = res;
            }
          )
        }
        this.availablePrice = this.rates[0].suggestedPrice +1;
      },
      err => {
        this.errorMessageRates = err.error;
        this.rates = [];
        this.availablePrice = Number(this.product.initialPrice.replace(",","."));
      }
    )
  }

}
