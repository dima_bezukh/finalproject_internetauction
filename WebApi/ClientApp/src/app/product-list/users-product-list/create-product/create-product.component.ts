import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountComponent } from 'src/app/account/account.component';
import { Category } from 'src/app/Models/category.model';
import { Product } from 'src/app/Models/product.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from 'src/app/Services/product.service';
import { ProductView } from 'src/app/Models/product-view.model';
import { UsersProductListComponent } from '../users-product-list.component';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  public categires: Category[] = [];
  productForm: FormGroup;
  public error: string;
  url: string;
  file: File;
  formData: FormData = new FormData();

  @ViewChild('labelImport', { static: true })
  labelImport: ElementRef;

  constructor(private productService: ProductService, private formBuilder: FormBuilder, 
              private account: AccountComponent, private router: Router,
              private toastr: ToastrService, private usersProducts: UsersProductListComponent) {
        account.getUser();         
  }
  
  ngOnInit() {
    this.productService.getAllCategories().subscribe(
      res => {
        this.categires = res;
      }
    );
    this.productForm = this.formBuilder.group({
        Name: ['', Validators.required],
        Description: ['', Validators.required],
        InitialPrice: ['', Validators.required],
        ClosingDate: ['', Validators.required],
        Category: ['', Validators.required],
        Image: ['', Validators.required]
    });
  }

  onSelectFile(event) {
    this.file = event.target.files[0];
    this.labelImport.nativeElement.innerText = this.file.name;
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event) => {
        this.url = reader.result as string;
      }
    }
}
  onSubmit(){
    this.formData.append("userId", this.account.user.id);
    this.formData.append("name", this.productForm.value.Name);
    this.formData.append("description", this.productForm.value.Description);
    this.formData.append("closingDate", this.productForm.value.ClosingDate);
    this.formData.append("categoryId", this.productForm.value.Category);
    this.formData.append("initialPrice", this.productForm.value.InitialPrice);
    this.formData.append("file", this.file);

    this.productService.createProduct(this.formData).subscribe(
      res => {
        this.formData = new FormData();
        this.error = '';
        this.toastr.success('Submitted successfully', 'Product was created');
        this.productForm.reset();
        this.url = '';
        this.labelImport.nativeElement.innerText = ' Choose file';
        this.usersProducts.refreshProducts();
      },
      err => {
        this.error = err.error;
        this.formData = new FormData();
      }
    );
  }
}
