import { Component, OnInit } from '@angular/core';
import { AccountComponent } from 'src/app/account/account.component';
import { Category } from 'src/app/Models/category.model';
import { ProductView } from 'src/app/Models/product-view.model';
import { ProductService } from 'src/app/Services/product.service';
import { CreateProductComponent} from './create-product/create-product.component';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/Models/product.model';

@Component({
  selector: 'app-users-product-list',
  templateUrl: './users-product-list.component.html',
  styleUrls: ['./users-product-list.component.css']
})
export class UsersProductListComponent implements OnInit {

  usersProducts: ProductView[] = [];
  errorMessage: string;
  isResizeble: boolean = false;

  constructor(private productService: ProductService, private account: AccountComponent,
              private toastr: ToastrService) {
              account.getUser();
   }

  ngOnInit() {
    this.refreshProducts();
  }

  refreshProducts(){
    const userId = localStorage.getItem("key");
    this.isResizeble = true;
    this.productService.getUsersProducts(userId).subscribe(
      res => {
        this.errorMessage = '';
        this.usersProducts = res;
      },
      err => {
        this.errorMessage = err.error;
        this.usersProducts = [];
      }
    );
  }

  isAvaliable(product: Product){
    return new Date(product.closingDate) > new Date();
  }
  
  onDelete(productId: number){
    if (confirm('Are you sure to delete this lot?')){
      this.productService.deleteUsersProduct(productId, this.account.user.id).subscribe(
        res => {
          this.toastr.info("Deleted successfully", 'Products');
          this.refreshProducts();
        },
        err => {
          this.toastr.error("Invalid Deleted", err.error);
        }
      );
    }
  }
}
