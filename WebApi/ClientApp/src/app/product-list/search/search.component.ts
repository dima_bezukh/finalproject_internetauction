import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Category } from 'src/app/Models/category.model';
import { Search } from 'src/app/Models/search.model';
import { ProductService } from 'src/app/Services/product.service';
import { ProductListComponent } from '../product-list.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public categires: Category[] = [];
  searchForm: FormGroup;
  searchModel: Search = new Search();
  constructor(private productService: ProductService, private formBuilder: FormBuilder,
    public productList: ProductListComponent) { }

  ngOnInit() {
    this.productService.getAllCategories().subscribe(
      res => {
        this.categires = res;
      }
    );
    this.searchForm = this.formBuilder.group({
      ProductName: [''],
      MaxInitialPrice: [0],
      MinInitialPrice: [0],
      Seller: [''],
      CategoryName: ['']
    });
  }

  onSubmit(){
    this.searchModel.categoryName = this.searchForm.value.CategoryName;
    this.searchModel.maxInitialPrice = this.searchForm.value.MaxInitialPrice;
    this.searchModel.minInitialPrice = this.searchForm.value.MinInitialPrice;
    this.searchModel.seller = this.searchForm.value.Seller;
    this.searchModel.productName = this.searchForm.value.ProductName;

    this.productService.getByFilter(this.searchModel).subscribe(
      res => {
        this.productList.errorMessage = '';
        this.productList.products = res;
      },
      err=> {
        this.productList.products = [];
        this.productList.errorMessage = err.error;
      }
    )
  }

}
