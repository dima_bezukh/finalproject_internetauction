import { Component, OnInit } from '@angular/core';
import { ProductView } from '../Models/product-view.model';
import { ProductService } from '../Services/product.service';
import { ProductDetailsComponent } from './product-details/product-details.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: ProductView[] = []
  errorMessage: string;

  constructor(private productService: ProductService, productDetails: ProductDetailsComponent) { }

  ngOnInit() {
    this.refreshList();
  }
  refreshList() {
    this.productService.getAllProducts().subscribe(
      res => {
        this.products = res;
      },
      err => {
        this.errorMessage = err.error;
      }
    )
  }
}
