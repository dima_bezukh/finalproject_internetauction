﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Business.Interfaces;
using Business.Services;
using Business.Models;
using Business.Validation;
using WebApi.Models;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public AccountController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }
        
        [HttpPost]
        [Route("~/api/Account/Register")]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            RegisterModel registerModel = _mapper.Map<RegisterViewModel, RegisterModel>(model);
            try
            {
                await _userService.Register(registerModel);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        [Route("~/api/Account/LogIn")]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            LoginModel loginModel = _mapper.Map<LoginViewModel, LoginModel>(model);
            try
            {
                UserModel user = await _userService.Login(loginModel);
                return Ok(_mapper.Map<UserModel, UserViewModel>(user));
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        [Authorize(Roles = "user, admin")]
        [Route("~/api/Account/LogOut")]
        public async Task<ActionResult> LogOut()
        {
            await _userService.LogOut();
            return Ok();
        }
        [HttpPut]
        [Authorize(Roles = "user, admin")]
        [Route("~/api/Account/RechargeBalance")]
        public async Task<IActionResult> RechargeBalance(PaymentDetailViewModel model)
        {
            try
            {
                await _userService.RechargeUsersBalance(_mapper.Map<PaymentDetailViewModel, PaymentDetailModel>(model));
                return Ok(model.AmountOfMoney);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Authorize(Roles = "admin")]
        [Route("~/api/Account/SetRolesToUser")]
        public async Task<ActionResult> SetUserRoles(string userId, List<string> roles)
        {
            try
            {
                await _userService.SetUserRoles(userId, roles);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("~/api/Account/GetName/{userId}")]
        public async Task<ActionResult<string>> GetUserName(string userId)
        {
            try
            {
                string name = await _userService.GetUserNameByIdAsync(userId);
                return name;
            }
            catch(Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "user, admin")]
        public async Task<ActionResult<UserViewModel>> Get(string id)
        {
            try
            {
                UserModel user = await _userService.GetUserByIdAsync(id);
                return _mapper.Map<UserViewModel>(user);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet]
        [Authorize(Roles = "user, admin")]
        [Route("~/api/Account/{id}/GetOrders")]
        public ActionResult<IEnumerable<OrderViewModel>> GetOrders(string id)
        {
            IEnumerable<OrderModel> orders = _userService.GetOrdersByUserId(id);
            if(!orders.Any())
            {
                return NotFound("You do not have any order");
            }
            return _mapper.Map<IEnumerable<OrderViewModel>>(orders).ToList();
        }
    }
}
