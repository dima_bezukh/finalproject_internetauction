﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Business.Interfaces;
using Business.Services;
using Business.Models;
using Business.Validation;
using WebApi.Models;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IImageService _imageService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public ProductsController(IProductService productService, IImageService imageService, 
                                  IUserService userService, IMapper mapper)
        {
            _productService = productService;
            _imageService = imageService;
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost]
        [Authorize(Roles = "user, admin")]
        public async Task<ActionResult> Create([FromForm] ProductCreateModel productViewModel)
        {
            int id = 0;
            try
            {
                ProductModel productModel = _mapper.Map<ProductCreateModel, ProductModel>(productViewModel);
                id = await _productService.AddAsync(productModel);

                var file = productViewModel.File;
                var folderName = Path.Combine("Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                if (file != null && file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    ImageModel imageModel = new ImageModel { ImageName = fileName, ProductId = id };
                    string newFileName = await _imageService.AddAsync(imageModel);
                    var fullPath = Path.Combine(pathToSave, newFileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
                else
                {
                    await _productService.DeleteByIdAsync(id);
                    return BadRequest("Image not valid");
                }
                return Ok();
            }
            catch (Exception ex)
            {
                if (id != 0)
                    await _productService.DeleteByIdAsync(id);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Authorize(Roles = "user, admin")]
        [Route("~/api/Products/GetUsersProducts/{userId}")]
        public async Task<ActionResult<IEnumerable<ProductViewModel>>> GetUsersProducts(string userId)
        {
            IEnumerable<ProductModel> products = _userService.GetUsersProducts(userId);
            if(!products.Any())
            {
                return NotFound("Your do not have any products!");
            }
            List<ProductViewModel> productViews = _mapper.Map<IEnumerable<ProductViewModel>>(await _productService.GetExtendedProductModels(products)).ToList();
            
            return productViews;
        }

        [HttpGet]
        [Authorize(Roles = "user, admin")]
        [Route("~/api/Products/GetWonProducts/{userId}")]
        public async Task<ActionResult<IEnumerable<ProductViewModel>>> GetWonProducts(string userId)
        {
            IEnumerable<ProductModel> products = _userService.GetWonLotsByUserId(userId);
            if (!products.Any())
            {
                return NotFound("Your have not won any lot!");
            }
            List<ProductViewModel> productViews = _mapper.Map<IEnumerable<ProductViewModel>>(await _productService.GetExtendedProductModels(products)).ToList();

            return productViews;
        }

        [HttpGet]
        [Route("~/api/Products/GetAll")]
        public async Task<ActionResult<IEnumerable<ProductViewModel>>> GetAll()
        {
            IEnumerable<ProductModel> products = _productService.GetAll();
            if (!products.Any())
            {
                return NotFound("There are no available products");
            }
            List<ProductViewModel> productViews = _mapper.Map<IEnumerable<ProductViewModel>>(await _productService.GetExtendedProductModels(products)).ToList();
            return productViews;
        }

        [HttpPost]
        [Route("~/api/Products/GetAllByFilter")]
        public async Task<ActionResult<IEnumerable<ProductViewModel>>> GetAllByFilter(FilterSearchViewModel filterSearch)
        {
            IEnumerable<ProductModel> products = _productService.GetByFilter(_mapper.Map<FilterSearchViewModel, FilterSearchModel>(filterSearch));
            if (!products.Any())
            {
                return BadRequest("There are no products that match the specified filter");
            }
            List<ProductViewModel> productViews = _mapper.Map<IEnumerable<ProductViewModel>>(await _productService.GetExtendedProductModels(products)).ToList();
            
            return productViews;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductViewModel>> GetById(int id)
        {
            ProductModel product = await _productService.GetByIdAsync(id);
            if (product == null)
                return NotFound("There is no product with such id!");

            return _mapper.Map<ProductViewModel>((await _productService.GetExtendedProductModels(new List<ProductModel> { product })).First());
        }

        [HttpGet]
        [Route("~/api/Products/{productId}/GetAllRates")]
        public ActionResult<IEnumerable<RateViewModel>> GetProductsRates(int productId)
        {
            IEnumerable<RateModel> rates = _productService.GetRatesByProductId(productId);
            if (rates.Any())
                return _mapper.Map<IEnumerable<RateModel>, IEnumerable<RateViewModel>>(rates).ToList();
            return
                NotFound("This product has no rates!");
        }

        [HttpDelete]
        [Authorize(Roles = "user, admin")]
        [Route("~/api/Products/DeleteProduct/{productId}/Owner/{ownerId}")]
        public async Task<ActionResult> Delete(int productId, string ownerId)
        {
            try
            {
                List<ImageModel> images = _productService.GetAllImagesByProductId(productId).ToList();
                await _productService.DeleteByIdAsyncUseOwnerId(productId, ownerId);

                for (int i=0; i < images.Count; i++)
                {
                    var folderName = Path.Combine("Images");
                    var pathTodelete =Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), folderName), images[i].ImageName);
                    System.IO.File.Delete(pathTodelete);
                }
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Authorize(Roles = "user, admin")]
        [Route("~/api/Products/{productId}/GetWinnigRate")]
        public async Task<ActionResult<RateViewModel>> GetWinnigRate(int productId)
        {
            try
            {
                RateModel rate = await _productService.GetWinnigRateByProductIdAsync(productId);
                return Ok(_mapper.Map<RateViewModel>(rate));
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
