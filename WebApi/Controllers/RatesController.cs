﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Business.Interfaces;
using Business.Services;
using Business.Models;
using Business.Validation;
using WebApi.Models;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class RatesController : ControllerBase
    {
        private readonly IRateService _rateService;
        private readonly IMapper _mapper;

        public RatesController(IRateService rateService, IMapper mapper)
        {
            _rateService = rateService;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "user, admin")]
        public async Task<ActionResult<RateViewModel>> Get(int id)
        {
            RateModel rate = await _rateService.GetByIdAsync(id);
            if(rate == null)
            {
                return NotFound("There is not any rates with such id");
            }

            return _mapper.Map<RateViewModel>(rate);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "user, admin")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _rateService.DeleteByIdAsync(id);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = "user, admin")]
        public async Task<ActionResult> Create(RateViewModel rateModel)
        {
            try
            {
                await _rateService.AddAsync(_mapper.Map<RateViewModel, RateModel>(rateModel));
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
