﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StatusesController : ControllerBase
    {
        private readonly IStatusService _statusService;

        public StatusesController(IStatusService statusService)
        {
            _statusService = statusService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<string>> GetName(int id)
        {
            StatusModel status = await _statusService.GetByIdAsync(id);
            if(status==null)
            {
                return NotFound("There is not any statuses with such id");
            }

            return status.Name;
        }
    }
}
