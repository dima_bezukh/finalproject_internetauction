﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Business.Interfaces;
using Business.Services;
using Business.Models;
using Business.Validation;
using WebApi.Models;
using System.IO;
using System.Net.Http.Headers;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;

        public CategoriesController(ICategoryService categoryService, IMapper mapper)
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }
        [HttpGet]
        public ActionResult<IEnumerable<CategoryViewModel>> Get()
        {
            IEnumerable<CategoryModel> categoryModels = _categoryService.GetAll();
            if(categoryModels.Any())
            {
                return _mapper.Map<IEnumerable<CategoryModel>, IEnumerable<CategoryViewModel>>(categoryModels).ToList();
            }
            else
            {
                return NotFound();
            }
        }

    }
}
