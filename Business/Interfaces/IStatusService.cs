﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Business.Models;

namespace Business.Interfaces
{
    public interface IStatusService
    {
        Task AddAsync(StatusModel model);
        Task DeleteByIdAsync(int modelId);
        IEnumerable<OrderModel> GetOrdersByStatusId(int statusId);
        Task<StatusModel> GetByIdAsync(int id);
    }
}
