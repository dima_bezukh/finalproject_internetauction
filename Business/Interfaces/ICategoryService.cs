﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Business.Models;

namespace Business.Interfaces
{
    public interface ICategoryService
    {
        Task<CategoryModel> GetByIdAsync(int id);
        IEnumerable<CategoryModel> GetAll();
        Task AddAsync(CategoryModel model);
        Task DeleteByIdAsync(int modelId);
    }
}
