﻿using Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IRateService
    {
        IEnumerable<RateModel> GetAll();
        Task<RateModel> GetByIdAsync(int id);
        Task AddAsync(RateModel model);
        Task DeleteByIdAsync(int modelId);
        void CloseExpiredLotsEveryDay();
    }
}
