﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;

namespace Business.Interfaces
{
    public interface IProductService: ICrud<ProductModel>
    {
        Task DeleteByIdAsyncUseOwnerId(int productId, string ownerId);
        IEnumerable<ImageModel> GetAllImagesByProductId(int productId);
        IEnumerable<ProductModel> GetByFilter(FilterSearchModel filterSearch);
        IEnumerable<RateModel> GetRatesByProductId(int productId);
        Task<IEnumerable<ExtendedProductModel>> GetExtendedProductModels(IEnumerable<ProductModel> products);
        Task<RateModel> GetWinnigRateByProductIdAsync(int productId);
    }
}
