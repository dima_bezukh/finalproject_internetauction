﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Business.Models;

namespace Business.Interfaces
{
    public interface IUserService
    {
        IEnumerable<ProductModel> GetWonLotsByUserId(string userId);
        IEnumerable<ProductModel> GetUsersProducts(string userId);
        Task RechargeUsersBalance(PaymentDetailModel model);
        IEnumerable<OrderModel> GetOrdersByUserId(string userId);
        Task SetUserRoles(string userId, List<string> roles);
        Task Register(RegisterModel model);
        Task<UserModel> Login(LoginModel model);
        Task LogOut();
        Task<string> GetUserNameByIdAsync(string userId);
        Task<UserModel> GetUserByIdAsync(string userId);
    }
}
