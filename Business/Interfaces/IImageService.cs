﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Business.Models;

namespace Business.Interfaces
{
    public interface IImageService
    {
        Task<string> AddAsync(ImageModel model);
        Task DeleteByIdAsync(int modelId);
    }
}
