﻿using System;

namespace Business.Validation
{
    [Serializable]
    public class InternetAuctionException: Exception
    {
        public InternetAuctionException() : base() { }
        public InternetAuctionException(string message) : base(message) { }
        public InternetAuctionException(string message, Exception inner) : base(message, inner) { }
    }
}
