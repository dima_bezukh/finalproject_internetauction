﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Business.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Balance { get; set; }
        public string[] RolesName { get; set; }

        public IEnumerable<int> ProductsIds { get; set; }
        public IEnumerable<int> RatesIds { get; set; }
    }
}
