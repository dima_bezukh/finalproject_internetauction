﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class StatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<int> OrdersIds { get; set; }
    }
}
