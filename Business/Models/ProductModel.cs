﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string InitialPrice { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime ClosingDate { get; set; }
        public int CategoryId { get; set; }
        public string UserId { get; set; }
        
        public IEnumerable<int> ImagesIds { get; set; }
        public IEnumerable<int> RatesIds { get; set; }
    }
}
