﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class ImageModel
    {
        public int Id { get; set; }
        public string ImageName { get; set; }
        public int ProductId { get; set; }
    }
}
