﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class PaymentDetailModel
    {
        public string UserId { get; set; }
        public string CardNumber { get; set; }
        public string ExpirationDate { get; set; }
        public string SecurityCode { get; set; }
        public string AmountOfMoney { get; set; }
    }
}
