﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class ExtendedProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string InitialPrice { get; set; }
        public DateTime ClosingDate { get; set; }
        public string[] ImagesNames { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string UserId { get; set; }
    }
}
