﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class RateModel
    {
        public int Id { get; set; }
        public bool IsWinning { get; set; }
        public decimal SuggestedPrice { get; set; }
        public DateTime CreatingDate { get; set; }
        public int ProductId { get; set; }
        public string UserId { get; set; }
    }
}
