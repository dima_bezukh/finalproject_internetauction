﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class FilterSearchModel
    {
        public string ProductName { get; set; }
        public decimal MaxInitialPrice { get; set; }
        public decimal MinInitialPrice { get; set; }
        public string Seller { get; set; }
        public string CategoryName { get; set; }
    }
}
