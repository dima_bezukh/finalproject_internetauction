﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public DateTime CreatingDate { get; set; }
        public string ShippingAddress { get; set; }
        public int StatusId { get; set; }
        public int RateId { get; set; }
    }
}
