﻿using AutoMapper;
using Data.Entities;
using Business.Models;
using System.Linq;
using System;

namespace Business
{
    public class AutomapperProfile: Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Category, CategoryModel>()
                .ForMember(p => p.ProductsIds, c => c.MapFrom(category => category.Products.Select(m => m.Id)))
                .ReverseMap();
            CreateMap<Image, ImageModel>()
                .ReverseMap();
            CreateMap<Product, ProductModel>()
                .ForMember(p => p.ImagesIds, c => c.MapFrom(product => product.Images.Select(m => m.Id)))
                .ForMember(p => p.RatesIds, c => c.MapFrom(product => product.Rates.Select(m => m.Id)))
                .ForMember(p=>p.InitialPrice, c => c.MapFrom(product => product.InitialPrice.ToString()))
                .ReverseMap()
                .ForMember(p=>p.InitialPrice, c=> c.MapFrom(productmodel => Convert.ToDecimal(productmodel.InitialPrice)));
            CreateMap<ProductModel, ExtendedProductModel>().ReverseMap();
            CreateMap<Status, StatusModel>()
                .ForMember(p => p.OrdersIds, c => c.MapFrom(status => status.Orders.Select(m => m.Id)))
                .ReverseMap();
            CreateMap<Rate, RateModel>()
                .ReverseMap();
            CreateMap<User, UserModel>()
                .ForMember(p => p.ProductsIds, c => c.MapFrom(user => user.Products.Select(m => m.Id)))
                .ForMember(p => p.RatesIds, c => c.MapFrom(user => user.Rates.Select(m => m.Id)));
            CreateMap<UserModel, User>()
                .ForMember(p => p.UserName, c => c.MapFrom(user => user.Email));
            CreateMap<RegisterModel, UserModel>()
                .ForMember(p=>p.Name, c=>c.MapFrom(user=>user.UserName))
                .ReverseMap();
            CreateMap<Order, OrderModel>()
                .ReverseMap();
        }
    }
}
