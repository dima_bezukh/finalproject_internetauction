﻿using System;
using System.Collections.Generic;
using System.Text;
using Business.Models;
using Business.Interfaces;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using System.Threading;

namespace Business.Services
{
    public class RateService : IRateService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public RateService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(RateModel model)
        {
            Product product = await _unitOfWork.ProductRepository.GetByIdAsync(model.ProductId);
            if (product == null)
                throw new InternetAuctionException("There is no product with such id!");
            if (product.ClosingDate < DateTime.Now)
                throw new InternetAuctionException("This product already closed!");
            if (model.SuggestedPrice < product.InitialPrice)
                throw new InternetAuctionException($"Suggested price should be greater or equal than {product.InitialPrice}$!");
            model.CreatingDate = DateTime.Now;
            model.IsWinning = false;
            var rates = _unitOfWork.RateRepository.FindAll().Where(p => p.ProductId == model.ProductId);
            if(rates.Any())
            {
                decimal MaxSuggestedPrice = rates.Select(n => n.SuggestedPrice).Max();
                if (model.SuggestedPrice <= MaxSuggestedPrice)
                    throw new InternetAuctionException("Suggested price should be greater than all other rates");
            }
            await _unitOfWork.RateRepository.AddAsync(_mapper.Map<RateModel, Rate>(model));
            await _unitOfWork.SaveAsync();
        }
        public void CloseExpiredLotsEveryDay()
        {
            Task.Run(async () =>
            {
                while(true)
                {
                    CloseExpiredLots();
                    double durationUntilTomorrow = CalcMilsecToNextDay();
                    await Task.Delay((int)durationUntilTomorrow);
                }
            });
        }
        private async void CloseExpiredLots()
        {
            List<Product> products = _unitOfWork.ProductRepository.FindAll().ToList();
            foreach(Product product in products)
            {
                if(product.ClosingDate <= DateTime.Now)
                {
                    await SetWinningRateByProductId(product.Id);
                    product.IsAvailable = false;
                    _unitOfWork.ProductRepository.Update(product);
                    await _unitOfWork.SaveAsync();
                }
            }
        }
        private double CalcMilsecToNextDay()
        {
            DateTime now = DateTime.Now;
            DateTime tomorrow = now.AddDays(1).AddMinutes(1).Date;

            double durationUntilTomorrow = (tomorrow - now).TotalMilliseconds;
            return durationUntilTomorrow;
        }
        public async Task<RateModel> SetWinningRateByProductId(int productId)
        {
            IEnumerable<Rate> allRates = _unitOfWork.RateRepository.FindAll().Where(p => p.ProductId == productId);
            if (!allRates.Any() || allRates == null)
                return null;
            Rate winnigRate = allRates.OrderByDescending(p => p.SuggestedPrice).First();
            winnigRate.IsWinning = true;

            _unitOfWork.RateRepository.Update(winnigRate);
            await _unitOfWork.SaveAsync();

            return _mapper.Map<Rate, RateModel>(winnigRate);
        }
        public async Task DeleteByIdAsync(int modelId)
        {
            Rate rate = await _unitOfWork.RateRepository.GetByIdWithDelailsAsync(modelId);
            if (rate.Product.ClosingDate < DateTime.Now)
                throw new InternetAuctionException("You can not delete this rate (this lot has already closed)");
            await _unitOfWork.RateRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<RateModel> GetAll()
        {
            IEnumerable<Rate> rates = _unitOfWork.RateRepository.FindAll();
            return _mapper.Map<IEnumerable<Rate>, IEnumerable<RateModel>>(rates);
        }

        public async Task<RateModel> GetByIdAsync(int id)
        {
            Rate rate = await _unitOfWork.RateRepository.GetByIdAsync(id);
            return _mapper.Map<Rate, RateModel>(rate);
        }
    }
}
