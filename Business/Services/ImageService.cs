﻿using System;
using System.Collections.Generic;
using System.Text;
using Business.Models;
using Business.Interfaces;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;

namespace Business.Services
{
    public class ImageService : IImageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ImageService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<string> AddAsync(ImageModel model)
        {
            if(await _unitOfWork.ProductRepository.GetByIdAsync(model.ProductId) == null)
                throw new InternetAuctionException("Invalid product id");
            if (string.IsNullOrEmpty(model.ImageName))
                throw new InternetAuctionException("Image name should not be empty");
            int index = model.ImageName.LastIndexOf(".");
            string fileName = model.ImageName.Substring(0, index);
            string format = model.ImageName.Substring(index + 1);
            string[] existsFormat = new string[] { "jpeg", "jpg", "png"};
            if (!existsFormat.Contains(format))
                throw new InternetAuctionException("Wrong file format");
            model.ImageName = fileName + "_" + DateTime.Now.ToString("yymmssfff") + "." + format;
            await _unitOfWork.ImageRepository.AddAsync(_mapper.Map<ImageModel, Image>(model));
            await _unitOfWork.SaveAsync();

            return model.ImageName;
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.ImageRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }
    }
}
