﻿using System;
using System.Collections.Generic;
using System.Text;
using Business.Models;
using Business.Interfaces;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using System.Text.RegularExpressions;

namespace Business.Services
{
    public class UserService : IUserService
    {
        private UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(UserManager<User> userManager, IUnitOfWork unitOfWork, IMapper mapper, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;

            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IEnumerable<ProductModel> GetUsersProducts(string userId)
        {
            IEnumerable<Product> products = _unitOfWork.ProductRepository.FindAll().Where(p => p.UserId == userId);
            return _mapper.Map<IEnumerable<Product>, IEnumerable<ProductModel>>(products);
        }
        public IEnumerable<OrderModel> GetOrdersByUserId(string userId)
        {
            IEnumerable<Order> orders = _unitOfWork.OrderRepository.FindAllWithDetails().Where(p => p.Rate.UserId == userId);
            return _mapper.Map<IEnumerable<Order>, IEnumerable<OrderModel>>(orders);
        }

        public IEnumerable<ProductModel> GetWonLotsByUserId(string userId)
        {
            IEnumerable<Product> products = _unitOfWork.RateRepository.FindAllWithDetails()
                                                                      .Where(p => p.IsWinning == true && p.UserId == userId &&
                                                                             !_unitOfWork.OrderRepository.FindAll().Select(n=>n.RateId).Contains(p.Id))
                                                                      .Select(pr => pr.Product);
            return _mapper.Map<IEnumerable<Product>, IEnumerable<ProductModel>>(products);
        }

        public async Task RechargeUsersBalance(PaymentDetailModel model)
        {
            User user = await _userManager.FindByIdAsync(model.UserId);

            if (user == null)
                throw new InternetAuctionException("Wrong user Id");
            if (!Regex.IsMatch(model.CardNumber, @"^\d{16}$"))
                throw new InternetAuctionException("Wrong card number");
            if (!Regex.IsMatch(model.ExpirationDate, @"^\d{2}\/\d{2}$"))
                throw new InternetAuctionException("Wrong expiration date");
            var date = model.ExpirationDate.Split("/");
            DateTime expdate = new DateTime(Convert.ToInt32("20"+date[1]), Convert.ToInt32(date[0]), 1);
            if(expdate.AddMonths(1) < DateTime.Now)
                throw new InternetAuctionException("Your card is expired");
            if (!Regex.IsMatch(model.SecurityCode, @"^\d{3}$"))
                throw new InternetAuctionException("Wrong security code");
            if(!Regex.IsMatch(model.AmountOfMoney, @"^\d{1,8}$"))
                throw new InternetAuctionException("Wrong format Amount Of Money");
            if (Convert.ToDecimal(model.AmountOfMoney) <= 0 )
                throw new InternetAuctionException("Amount of money should be greated than 0");

            user.Balance += Convert.ToDecimal(model.AmountOfMoney);
            await _userManager.UpdateAsync(user);
        }

        public async Task SetUserRoles(string userId, List<string> roles)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new InternetAuctionException("Wrong user Id");

            var userRoles = await _userManager.GetRolesAsync(user);
            var addedRoles = roles.Except(userRoles);
            var removedRoles = userRoles.Except(roles);

            await _userManager.AddToRolesAsync(user, addedRoles);
            await _userManager.RemoveFromRolesAsync(user, removedRoles);
        }
        public async Task Register(RegisterModel model)
        {
            if (!IsCorrectEmail(model.Email))
                throw new InternetAuctionException("Wrong email");
            if (model.Password != model.PasswordConfirm)
                throw new InternetAuctionException("Password and confirmed password should be equal");
            UserModel userModel = _mapper.Map<RegisterModel, UserModel>(model);
            userModel.Balance = 0;
            User user = _mapper.Map<UserModel, User>(userModel);
            user.Id = Guid.NewGuid().ToString();
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "user");
            }
            else
            {
                string errorMessage = "";
                foreach (var error in result.Errors)
                {
                    errorMessage += error.Description + " ";
                }
                throw new InternetAuctionException(errorMessage);
            }
        }
        public async Task<UserModel> Login(LoginModel model)
        {
            if (string.IsNullOrEmpty(model.Email))
                throw new InternetAuctionException("Email can not be empty");
            if (string.IsNullOrEmpty(model.Password))
                throw new InternetAuctionException("Password can not be empty");
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, true, false);
            if (result.Succeeded)
            {
                User user = await _userManager.FindByEmailAsync(model.Email);
                UserModel userModel = _mapper.Map<User, UserModel>(user);
                userModel.RolesName = (await _userManager.GetRolesAsync(user)).ToArray();
                return userModel;
            }
            else
                throw new InternetAuctionException("Wrong email or (and) password!");
        }
        public async Task LogOut()
        {
            await _signInManager.SignOutAsync();
        }
        private bool IsCorrectEmail(string email)
        {
            string pattern = @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(email);
        }

        public async Task<string> GetUserNameByIdAsync(string userId)
        {
            UserModel user = await GetUserByIdAsync(userId);
            return user.Name;
        }

        public async Task<UserModel> GetUserByIdAsync(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new InternetAuctionException("There is no user with such id");
            UserModel userModel = _mapper.Map<UserModel>(user);
            userModel.RolesName = (await _userManager.GetRolesAsync(user)).ToArray();
            return userModel;
        }
    }
}
