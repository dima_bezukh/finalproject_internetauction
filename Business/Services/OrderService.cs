﻿using System;
using System.Collections.Generic;
using System.Text;
using Business.Models;
using Business.Interfaces;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace Business.Services
{
    public class OrderService : IOrderService
    {
        private UserManager<User> _userManager;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public OrderService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<User> userManager)
        {
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<int> AddAsync(OrderModel model)
        {
            Rate rate = _unitOfWork.RateRepository.FindAll().FirstOrDefault(p => p.IsWinning && p.Id == model.RateId);
            if (rate == null)
                throw new InternetAuctionException("Wrong rate id");
            User user = await _userManager.FindByIdAsync(rate.UserId);
            if (user == null)
                throw new InternetAuctionException("Wrong user id");
            if (user.Balance < rate.SuggestedPrice)
                throw new InternetAuctionException("You do no have enough money. Recharge your balance and try again!");
            if (string.IsNullOrEmpty(model.ShippingAddress))
                throw new InternetAuctionException("Shipping address should not be empty");
            if(_unitOfWork.OrderRepository.FindAll().Any(p=>p.RateId==model.RateId))
                throw new InternetAuctionException("For this lot order already exists");

            user.Balance -= rate.SuggestedPrice;
            await _userManager.UpdateAsync(user);

            model.CreatingDate = DateTime.Now;
            model.StatusId = _unitOfWork.StatustRepository.FindAll().FirstOrDefault(p => p.Name.ToLower() == "created")?.Id ?? 1;
            Order order = _mapper.Map<OrderModel, Order>(model);

            await _unitOfWork.OrderRepository.AddAsync(order);
            await _unitOfWork.SaveAsync();

            return order.Id;
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.OrderRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<OrderModel> GetAll()
        {
            IEnumerable<Order> orders = _unitOfWork.OrderRepository.FindAll();
            return _mapper.Map<IEnumerable<Order>, IEnumerable<OrderModel>>(orders);
        }

        public async Task<OrderModel> GetByIdAsync(int id)
        {
            Order order = await _unitOfWork.OrderRepository.GetByIdAsync(id);
            return _mapper.Map<Order, OrderModel>(order);
        }

        public async Task UpdateAsync(OrderModel model)
        {
            if (string.IsNullOrEmpty(model.ShippingAddress))
                throw new InternetAuctionException("Shipping address should not be empty");
            if (!_unitOfWork.RateRepository.FindAll().Any(p => p.IsWinning && p.Id == model.RateId))
                throw new InternetAuctionException("Wrong rate id or this rate is not winning");
            if (!_unitOfWork.StatustRepository.FindAll().Any(p => p.Id == model.StatusId))
                throw new InternetAuctionException("Wrong input category id");

            _unitOfWork.OrderRepository.Update(_mapper.Map<OrderModel, Order>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}
