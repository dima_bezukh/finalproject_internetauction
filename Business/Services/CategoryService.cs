﻿using System;
using System.Collections.Generic;
using System.Text;
using Business.Models;
using Business.Interfaces;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;

namespace Business.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task AddAsync(CategoryModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
                throw new InternetAuctionException("Category name should not be empty");
            if (_unitOfWork.CategoryRepository.FindAll().Any(p => p.Name.ToLower() == model.Name.ToLower()))
                throw new InternetAuctionException("Category with such name already exists");
            await _unitOfWork.CategoryRepository.AddAsync(_mapper.Map<CategoryModel, Category>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.CategoryRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<CategoryModel> GetAll()
        {
            IEnumerable<Category> categories = _unitOfWork.CategoryRepository.FindAll();
            return _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryModel>>(categories);
        }

        public async Task<CategoryModel> GetByIdAsync(int id)
        {
            return _mapper.Map<Category, CategoryModel>(await _unitOfWork.CategoryRepository.GetByIdAsync(id));
        }
    }
}
