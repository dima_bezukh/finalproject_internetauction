﻿using System;
using System.Collections.Generic;
using System.Text;
using Business.Models;
using Business.Interfaces;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;

namespace Business.Services
{
    public class StatusService : IStatusService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public StatusService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task AddAsync(StatusModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
                throw new InternetAuctionException("Status name should not be empty");
            if(_unitOfWork.StatustRepository.FindAll().Any(p=>p.Name==model.Name))
                throw new InternetAuctionException("Status with such name already exists");
            await _unitOfWork.StatustRepository.AddAsync(_mapper.Map<StatusModel, Status>(model));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.StatustRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<StatusModel> GetByIdAsync(int id)
        {
            Status status = await _unitOfWork.StatustRepository.GetByIdAsync(id);
            return _mapper.Map<StatusModel>(status);
        }

        public IEnumerable<OrderModel> GetOrdersByStatusId(int statusId)
        {
            IEnumerable<Order> orders = _unitOfWork.OrderRepository.FindAll().Where(p => p.StatusId == statusId);
            return _mapper.Map<IEnumerable<Order>, IEnumerable<OrderModel>>(orders);
        }
    }
}
