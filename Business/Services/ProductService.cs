﻿using System;
using System.Collections.Generic;
using System.Text;
using Business.Models;
using Business.Interfaces;
using Business.Validation;
using Data.Entities;
using Data.Interfaces;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using System.Text.RegularExpressions;

namespace Business.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<int> AddAsync(ProductModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
                throw new InternetAuctionException("Product name should not be empty");
            if (!Regex.IsMatch(model.InitialPrice, @"^\d{1,8}$"))
                throw new InternetAuctionException("Wrong format of initial price");
            if (Convert.ToDecimal(model.InitialPrice) <= 0)
                throw new InternetAuctionException("Initial Price should be greated than 0");
            if (model.ClosingDate < DateTime.Now.AddDays(1))
                throw new InternetAuctionException("Duration of time before closing the lot should be greater than 1 day");
            if (!_unitOfWork.CategoryRepository.FindAll().Any(p => p.Id == model.CategoryId))
                throw new InternetAuctionException("There is not such category");
            Product product = _mapper.Map<ProductModel, Product>(model);
            product.IsAvailable = true;

            await _unitOfWork.ProductRepository.AddAsync(product);
            await _unitOfWork.SaveAsync();

            return product.Id;
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            if(await _unitOfWork.ProductRepository.GetByIdAsync(modelId) == null)
                throw new InternetAuctionException("There is no product with such id");
            await _unitOfWork.ProductRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsyncUseOwnerId(int productId, string ownerId)
        {
            Product product = await _unitOfWork.ProductRepository.GetByIdAsync(productId);
            if(product == null)
                throw new InternetAuctionException("There is no product with such id");
            if (product.ClosingDate < DateTime.Now)
                throw new InternetAuctionException("This product already closed!");
            if (product.UserId != ownerId)
                throw new InternetAuctionException("You are not owner of this product");
            if (_unitOfWork.RateRepository.FindAll().Where(p => p.ProductId == productId).Any())
                throw new InternetAuctionException("You can not delete this product (This product has at least one rate)");

            await _unitOfWork.ProductRepository.DeleteByIdAsync(productId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<ProductModel> GetAll()
        {
            IEnumerable<Product> products = _unitOfWork.ProductRepository.FindAll().Where(p=>p.IsAvailable);
            return _mapper.Map<IEnumerable<Product>, IEnumerable<ProductModel>>(products);
        }

        public IEnumerable<ImageModel> GetAllImagesByProductId(int productId)
        {
            IEnumerable<Image> images = _unitOfWork.ImageRepository.FindAll().Where(p=>p.ProductId==productId);
            return _mapper.Map<IEnumerable<Image>, IEnumerable<ImageModel>>(images);
        }

        public IEnumerable<ProductModel> GetByFilter(FilterSearchModel filterSearch)
        {
            IEnumerable<Product> products = _unitOfWork.ProductRepository.FindAllWithDetails().Where(p=>p.IsAvailable);
            if (!string.IsNullOrEmpty(filterSearch.CategoryName))
                products = products.Where(p => p.Category.Name.ToLower() == filterSearch.CategoryName.ToLower());
            if (!string.IsNullOrEmpty(filterSearch.Seller))
                products = products.Where(p => p.User.Name.ToLower() == filterSearch.Seller.ToLower());
            if (!string.IsNullOrEmpty(filterSearch.ProductName))
                products = products.Where(p => p.Name.ToLower() == filterSearch.ProductName.ToLower());
            if (filterSearch.MinInitialPrice > 0)
                products = products.Where(p => p.InitialPrice >= filterSearch.MinInitialPrice);
            if (filterSearch.MaxInitialPrice > 0)
                products = products.Where(p => p.InitialPrice <= filterSearch.MaxInitialPrice);
            return _mapper.Map<IEnumerable<Product>, IEnumerable<ProductModel>>(products);
        }

        public async Task<ProductModel> GetByIdAsync(int id)
        {
            Product product = await _unitOfWork.ProductRepository.GetByIdAsync(id);
            return _mapper.Map<Product, ProductModel>(product);
        }

        public async Task<IEnumerable<ExtendedProductModel>> GetExtendedProductModels(IEnumerable<ProductModel> products)
        {
            List<ExtendedProductModel> extendedProducts = new List<ExtendedProductModel>();
            foreach(var product in products)
            {
                ExtendedProductModel extendedProduct = _mapper.Map<ProductModel, ExtendedProductModel>(product);
                extendedProduct.CategoryName = (await _unitOfWork.CategoryRepository.GetByIdAsync(product.CategoryId))?.Name;

                var images = _unitOfWork.ImageRepository.FindAll().Where(p => p.ProductId == product.Id);
                if (images.Any())
                    extendedProduct.ImagesNames = images.Select(p => p.ImageName).ToArray();

                extendedProducts.Add(extendedProduct);
            }
            return extendedProducts;
        }

        public IEnumerable<RateModel> GetRatesByProductId(int productId)
        {
            IEnumerable<Rate> rates = _unitOfWork.RateRepository.FindAll().Where(p => p.ProductId == productId);
            return _mapper.Map<IEnumerable<Rate>, IEnumerable<RateModel>>(rates);
        }

        public async Task<RateModel> GetWinnigRateByProductIdAsync(int productId)
        {
            Product product = await _unitOfWork.ProductRepository.GetByIdAsync(productId);
            if (product == null)
                throw new InternetAuctionException("There is no product with such id");
            if (product.ClosingDate >= DateTime.Now)
                throw new InternetAuctionException("This product still open!");
            IEnumerable<Rate> allRates = _unitOfWork.RateRepository.FindAll().Where(p => p.ProductId == productId);
            if (!allRates.Any() || allRates == null)
                throw new InternetAuctionException("This product has no rates!");
            Rate winnigRate = allRates.OrderByDescending(p => p.SuggestedPrice).First();

            return _mapper.Map<RateModel>(winnigRate);
        }

        public async Task UpdateAsync(ProductModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
                throw new InternetAuctionException("Product name should not be empty");
            if (!Regex.IsMatch(model.InitialPrice, @"^\d{1,8}$"))
                throw new InternetAuctionException("Wrong format of initial price");
            if (Convert.ToDecimal(model.InitialPrice) <= 0)
                throw new InternetAuctionException("Initial Price should be greated than 0");
            if (model.ClosingDate < DateTime.Now.AddDays(1))
                throw new InternetAuctionException("Duration of time before closing the lot should be greater than 1 day");
            if (!_unitOfWork.CategoryRepository.FindAll().Any(p => p.Id == model.CategoryId))
                throw new InternetAuctionException("There is not such category");

            _unitOfWork.ProductRepository.Update(_mapper.Map<ProductModel, Product>(model));
            await _unitOfWork.SaveAsync();
        }
    }
}
